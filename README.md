Shard
=========

Shard is the Erlang-based server backbone for an MMO server running over
WebSockets.

Build Instructions
------------------
* (If your operating system is not 64-bit Linux, copy your premake
executable into the `physics` folder and follow the instructions
on the premake website for your environment. Visual Studio is not
supported at this time.)

###Dependencies:
* Make sure you have bullet3's source directory located
at `/usr/lib/bullet3/src`
* Make sure you have Erlang libs located at `/usr/lib/erlang/lib`
* Make sure you have rebar in your path.

###Building the project:
Run the following commands:

```sh
cd physics
./premake4_linux64 gmake
make config=release
cd ..
rebar getdeps
rebar compile
```

Running the server
------------------

```sh
./start.sh
```


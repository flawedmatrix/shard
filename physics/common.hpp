/* common.hpp */

#pragma once

#include <iostream>
#include <stdint.h>
#include "variant.hpp"

using byte = uint8_t;
using uint = unsigned int;

// MovementState enum
enum class MovementState : byte {
    UP     = (1u << 0),
    LEFT   = (1u << 1),
    DOWN   = (1u << 2),
    RIGHT  = (1u << 3),
    WAT    = (1u << 7)
};

inline byte operator&(byte a, MovementState b)
{
    return a & static_cast<byte>(b);
}

// This defines the TaskID enum in a more DRY way
#define _TASKID_LIST(m) \
        m(ADD_CHAR),    \
        m(RM_CHAR),     \
        m(MOVE_CHAR),   \
        m(ORIENT_CHAR), \
        m(JUMP_CHAR),   \
        m(FOO),         \
        m(WAT)

#define _TASKID_VALUE(v) v

enum class TaskID : uint {
    _TASKID_LIST(_TASKID_VALUE)
};

namespace std
{
    template<>
        struct hash<TaskID>
        {
            using underlying_type = std::underlying_type<TaskID>::type;
            using result_type = std::hash<underlying_type>::result_type;
            result_type operator()(const TaskID& arg) const
            {
                std::hash<underlying_type> hasher;
                return hasher(static_cast<underlying_type>(arg));
            }
        };
}

// Task structures
template <TaskID t>
struct Task {
};

template <>
struct Task<TaskID::ADD_CHAR> {
    uint id;
};

template <>
struct Task<TaskID::RM_CHAR> {
    uint id;
};

template <>
struct Task<TaskID::MOVE_CHAR> {
    uint id;
    MovementState dir;
    bool on;
};

template <>
struct Task<TaskID::ORIENT_CHAR> {
    uint id;
    double dirx;
    double diry;
    double dirz;
};

template <>
struct Task<TaskID::JUMP_CHAR> {
    uint id;
};

template <>
struct Task<TaskID::FOO> {
    int data;
};


template <TaskID... T>
class TaskVariant : util::variant<Task<T>...> {

    TaskID tid;

public:
    TaskVariant():
        util::variant<Task<T>...>(), tid(TaskID::WAT) {}

    TaskVariant(util::no_init x):
        util::variant<Task<T>...>(x), tid(TaskID::WAT) {}

    template <TaskID U>
    explicit TaskVariant(Task<U> const& val) noexcept :
        util::variant<Task<T>...>(std::forward<Task<U>>(val)), tid(U) {}

    template <TaskID U>
    explicit TaskVariant(Task<U>&& val) noexcept :
        util::variant<Task<T>...>(std::forward<Task<U>>(val)), tid(U) {}

    TaskVariant(TaskVariant<T...> const& old):
        util::variant<Task<T>...>(old),
        tid(old.tid)
    {}

    TaskVariant(TaskVariant<T...>&& old) noexcept:
        util::variant<Task<T>...>(std::move(old)),
        tid(old.tid)
    {}

    friend void swap(TaskVariant<T...>& first, TaskVariant<T...>& second)
    {
        using std::swap; //enable ADL
        util::variant<Task<T>...>& a = first;
        util::variant<Task<T>...>& b = second;
        swap(a, b);
        swap(first.tid, second.tid);
    }

    TaskVariant<T...>& operator=(TaskVariant<T...> other)
    {
        swap(*this, other);
        return *this;
    }

    // conversions
    // move-assign
    template <TaskID U>
    TaskVariant<T...>& operator=(Task<U>&& rhs) noexcept
    {
        TaskVariant<T...> temp(std::move(rhs));
        swap(*this, temp);
        return *this;
    }

    // copy-assign
    template <TaskID U>
    TaskVariant<T...>& operator=(Task<U> const& rhs)
    {
        TaskVariant<T...> temp(rhs);
        swap(*this, temp);
        return *this;
    }

    template<TaskID U>
    bool is() const
    {
        return util::variant<Task<T>...>::template is<Task<U>>();
    }

    bool valid() const
    {
        return util::variant<Task<T>...>::valid();
    }

    template<TaskID U, typename... Args>
    void set(Args&&... args)
    {
        util::variant<Task<T>...>::template
            set<Task<U>>(std::forward<Args>(args)...);
        tid = U;
    }

    template<TaskID U>
    Task<U>& get()
    {
        return util::variant<Task<T>...>::template get<Task<U>>();
    }

    template<TaskID U>
    Task<U> const& get() const
    {
        return util::variant<Task<T>...>::template get<Task<U>>();
    }

    TaskID get_tid() const
    {
        return tid;
    }

};

#define _TASKID_VALUE2(v) TaskID::v
using Job = TaskVariant<_TASKID_LIST(_TASKID_VALUE2)>;

#ifdef DEBUG
template <typename T>
void debug_print(T t)
{
    std::cerr << t << std::endl;
}

template <typename T, typename... Args>
void debug_print(T t, Args... args)
{
    std::cerr << t;
    debug_print(args...);
}
#endif

#ifdef DEBUG
    #define DPRINT(...) \
        debug_print("[DEBUG] In ", __FILE__, ":", __LINE__, ": ", __VA_ARGS__)
#else
    #define DPRINT(...)
#endif


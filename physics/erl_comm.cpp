/* erl_comm.cpp */

#include <iostream>
#include "erl_comm.hpp"

bool erlio::read_cmd(uint8_t* buf)
{
    // Read first two bytes to get the length of the message
    std::cin.read(reinterpret_cast<char*>(buf), 2);
    if (std::cin.eof()) {
        return false;
    }
    if (!std::cin.good()) {
        std::cerr << "Unable to read length from cin!" << std::endl;
        return false;
    }
    uint len = (static_cast<uint>(buf[0]) << 8) | static_cast<uint>(buf[1]);

    // Read the rest of the message
    std::cin.read(reinterpret_cast<char*>(buf), len);
    if (!std::cin.good()) {
        std::cerr << "Unable to read " << len << " bytes!" << std::endl;
        return false;
    }
    return true;
}

void erlio::write_cmd(uint8_t* buf, uint len)
{
    std::lock_guard<std::mutex> lock(iomtx);
    len &= 0xFFFF;
    std::cout << static_cast<byte>(len >> 8);
    std::cout << static_cast<byte>(len);
    std::cout.write(reinterpret_cast<char*>(buf), len);
    std::cout.flush();
}


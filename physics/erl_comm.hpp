/* erl_comm.hpp */
#pragma once
#include <memory>
#include <mutex>
#include <array>
#include "common.hpp"
#include "eterm.hpp"

namespace erlio {
    namespace {
        std::mutex iomtx;
    }

    bool read_cmd(byte* buf);
    void write_cmd(byte* buf, uint len);
    template<size_t N>
    void write_term(std::array<uint8_t, N>& buf, eterm& term)
    {
        term.encode<N>(buf);
        write_cmd(buf.data(), term.get_size());
    }
}


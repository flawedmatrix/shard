/* eterm.hpp */
#pragma once

#include <iostream>
#include <string>
#include <array>
#include <vector>
#include <algorithm>
#include <memory>
#include <stdexcept>
#include <stdint.h>
#include "variant.hpp"

/*
    The only type ids from http://www.erlang.org/doc/apps/erts/erl_ext_dist.html
    that we need
*/
enum erl_type {
    SMALL_INTEGER_EXT = 97,
    INTEGER_EXT = 98,
    OLD_FLOAT_EXT = 99,
    FLOAT_EXT = 70,
    ATOM_EXT = 100,
    TUPLE_EXT = 104,
    LARGE_TUPLE_EXT = 105,
    NIL_EXT = 106,
    STRING_EXT = 107,
    LIST_EXT = 108,
    BINARY_EXT = 109
};

#define _ERL_VERSION 131

namespace detail {
    template<size_t N>
    inline void copy_bytes(
        const std::array<uint8_t, N>& input,
        size_t begin,
        size_t num,
        void* dest
    )
    {
        std::reverse_copy(
            input.begin() + begin,
            input.begin() + begin + num,
            reinterpret_cast<uint8_t*>(dest)
        );
    }

    template<size_t N>
    inline void copy_bytes(
        const void* src,
        size_t begin,
        size_t num,
        std::array<uint8_t, N>& dest
    )
    {
        std::reverse_copy(
            reinterpret_cast<const uint8_t*>(src),
            reinterpret_cast<const uint8_t*>(src) + num,
            dest.begin() + begin
        );
    }
};

class eterm {
public:
    using small_int_t = int32_t;
    using integer_t = int32_t;
    using float_t = double;
    using atom_t = std::string;
    using tuple_t = std::vector<eterm>;
    using nil_t = std::vector<eterm>;
    using string_t = std::string;
    using list_t = std::vector<eterm>;
    using binary_t = std::vector<uint8_t>;

private:
    using data_t = util::variant<
        integer_t, // SMALL INTEGER or INTEGER
        float_t, // FLOAT
        std::string, // ATOM or STRING
        std::vector<eterm>, // TUPLE or LIST or NIL
        binary_t // BINARY
    >;

    std::shared_ptr<data_t> data;
    erl_type etype;
    size_t encoded_size;

public:
    eterm():
        data(std::make_shared<data_t>()),
        etype(NIL_EXT),
        encoded_size(1)
    { }

    eterm(const integer_t v):
        data(std::make_shared<data_t>(v))
    {
        if (static_cast<uint32_t>(v) < 0x100) {
            etype = SMALL_INTEGER_EXT;
            encoded_size = 2;
        } else {
            etype = INTEGER_EXT;
            encoded_size = 5;
        }
    }

    eterm(const float_t v):
        data(std::make_shared<data_t>(v)),
        etype(FLOAT_EXT),
        encoded_size(9)
    { }

    eterm(erl_type et, const std::string v):
        data(std::make_shared<data_t>(v)),
        etype(et),
        encoded_size(3 + v.length())
    { }

    // Versions of the container constructors that do not take in a length
    eterm(erl_type et, const std::vector<eterm> v):
        data(std::make_shared<data_t>(v)),
        etype(et)
    {
        size_t len = 0;
        const std::vector<eterm>& e = get<std::vector<eterm>>();
        for (auto & el : e) {
            len += el.encoded_size;
        }
        if (et == TUPLE_EXT) {
            encoded_size = 2 + len;
        } else {
            encoded_size = 5 + len;
        }
    }

    // Versions of the container constructors that take in a length
    eterm(erl_type et, const std::vector<eterm> v, size_t len):
        data(std::make_shared<data_t>(v)),
        etype(et)
    {
        if (et == TUPLE_EXT) {
            encoded_size = 2 + len;
        } else {
            encoded_size = 5 + len;
        }
    }

    eterm(const binary_t v):
        data(std::make_shared<data_t>(v)),
        etype(BINARY_EXT),
        encoded_size(5 + v.size())
    { }

    erl_type get_type() const
    {
        return etype;
    }

    size_t get_size() const
    {
        return 1 + encoded_size;
    }

    template<typename T>
    T const& get() const
    {
        return data->get<T>();
    }

    friend std::ostream& operator<<(std::ostream& stream, const eterm& et)
    {
        et.print(stream);
        return stream;
    }

    void print(std::ostream& stream) const
    {
        switch (etype) {
        case SMALL_INTEGER_EXT: {
            stream << get<integer_t>();
            break;
        }
        case INTEGER_EXT: {
            stream << get<integer_t>();
            break;
        }
        case FLOAT_EXT: {
            stream << get<float_t>();
            break;
        }
        case STRING_EXT:
            // Same as below case
        case ATOM_EXT: {
            stream << get<std::string>();
            break;
        }
        case NIL_EXT: {
            stream << "[]";
            break;
        }
        case BINARY_EXT: {
            const std::vector<uint8_t>& binary_v = get<binary_t>();
            stream << "<<";
            for (auto & b: binary_v) {
                stream << static_cast<uint32_t>(b) << ",";
            }
            stream << "\b>>";
            break;
        }
        case TUPLE_EXT:
            // Fall through
        case LARGE_TUPLE_EXT: {
            const std::vector<eterm>& tuple_v = get<tuple_t>();
            stream << "{";
            for (auto & el: tuple_v) {
                el.print(stream);
                stream << ",";
            }
            stream << "\b}";
            break;
        }
        case LIST_EXT: {
            const std::vector<eterm>& list_v = get<list_t>();
            stream << "[";
            for (auto & el: list_v) {
                el.print(stream);
                stream << ",";
            }
            stream << "\b]";
            break;
        }
        default:
            throw std::runtime_error("Invalid eterm");
        }
    }

    template<size_t N>
    static inline size_t parse_sub(
        const std::array<uint8_t, N> input,
        size_t begin,
        size_t num,
        std::vector<eterm>& e
    )
    {
        size_t size = 0;
        for (size_t i = 0; i < num; i += 1) {
            e.push_back(parse<N>(input, begin + size));
            size += e.back().encoded_size;
        }
        return size;
    }

    template<size_t N>
    static eterm parse(const std::array<uint8_t, N>& input)
    {
        return parse<N>(input, 0);
    }

    template<size_t N>
    static eterm parse(const std::array<uint8_t, N>& input, size_t i)
    {
        if (i == 0) {
            if (input[0] == _ERL_VERSION) {
                return parse<N>(input, 1);
            }
            throw std::runtime_error("Invalid eterm");
        }
        switch (input[i]) {
        case SMALL_INTEGER_EXT: {
            return eterm(static_cast<int8_t>(input[i + 1]));
        }
        case INTEGER_EXT: {
            int32_t int_v = 0;
            detail::copy_bytes<N>(input, i + 1, 4, &int_v);
            return eterm(int_v);
        }
        case FLOAT_EXT: {
            double float_v = 0;
            detail::copy_bytes<N>(input, i + 1, 8, &float_v);
            return eterm(float_v);
        }
        case STRING_EXT:
            // Handled the same way as below.
        case ATOM_EXT: {
            size_t len = (input[i + 1] << 8) + input[i + 2];
            return eterm(ATOM_EXT, std::string(
                input.begin() + i + 3,
                input.begin() + i + 3 + len
            ));
        }
        case NIL_EXT:
            return eterm();
        case BINARY_EXT: {
            uint32_t size = 0;
            detail::copy_bytes<N>(input, i + 1, 4, &size);
            return eterm(std::vector<uint8_t>(
                input.begin() + i + 5,
                input.begin() + i + 5 + size
            ));
        }
        case TUPLE_EXT: {
            size_t arity = input[i + 1];

            std::vector<eterm> e;
            size_t size = parse_sub<N>(input, i + 2, arity, e);
            return eterm(TUPLE_EXT, e, size);
        }
        case LARGE_TUPLE_EXT: {
            uint32_t arity = 0;
            detail::copy_bytes<N>(input, i + 1, 4, &arity);

            std::vector<eterm> e;
            size_t size = parse_sub<N>(input, i + 5, arity, e);
            return eterm(LARGE_TUPLE_EXT, e, size);
        }
        case LIST_EXT: {
            uint32_t num_elems = 0;
            detail::copy_bytes<N>(input, i + 1, 4, &num_elems);

            std::vector<eterm> e;
            size_t size = parse_sub<N>(input, i + 5, num_elems, e);
            if (input[i + 5 + size] == NIL_EXT) {
                size += 1;
            } else {
                e.push_back(parse<N>(input, i + 5 + size));
                size += e.back().encoded_size;
            }
            return eterm(LIST_EXT, e, size);
        }
        case OLD_FLOAT_EXT:
            throw std::runtime_error("Old style floats not supported!");
            break;
        default:
            throw std::runtime_error("Invalid eterm!");
        }
    }

    template<size_t N>
    inline void encode_sub(std::array<uint8_t, N>& output, size_t begin) const
    {
        const std::vector<eterm>& e = get<std::vector<eterm>>();
        size_t size = 0;
        for (auto & el : e) {
            el.encode(output, begin + size);
            size += el.encoded_size;
        }
    }

    template<size_t N>
    void encode(std::array<uint8_t, N>& output) const
    {
        encode<N>(output, 0);
    }

    template<size_t N>
    void encode(std::array<uint8_t, N>& output, size_t i) const
    {
        if (i == 0) {
            output[0] = _ERL_VERSION;
            encode(output, 1);
            return;
        }
        output[i] = etype;
        switch (etype) {
        case SMALL_INTEGER_EXT: {
            const int32_t& int_v = get<integer_t>();
            output[i + 1] = static_cast<int8_t>(int_v);
            break;
        }
        case INTEGER_EXT: {
            const int32_t& int_v = get<integer_t>();
            detail::copy_bytes<N>(&int_v, i + 1, 4, output);
            break;
        }
        case FLOAT_EXT: {
            const double& float_v = get<float_t>();
            detail::copy_bytes<N>(&float_v, i + 1, 8, output);
            break;
        }
        case STRING_EXT:
            // Same method as below
        case ATOM_EXT: {
            const std::string& atom_v = get<std::string>();
            const uint16_t size = atom_v.length();
            detail::copy_bytes<N>(&size, i + 1, 2, output);
            std::copy(atom_v.begin(), atom_v.end(), output.begin() + i + 3);
            break;
        }
        case NIL_EXT: {
            break;
        }
        case BINARY_EXT: {
            const std::vector<uint8_t>& binary_v = get<binary_t>();
            const uint32_t size = binary_v.size();
            detail::copy_bytes<N>(&size, i + 1, 4, output);
            std::copy(binary_v.begin(), binary_v.end(), output.begin() + i + 5);
            break;
        }
        case TUPLE_EXT: {
            const uint8_t arity = get<tuple_t>().size();
            output[i + 1] = arity;
            encode_sub<N>(output, i + 2);
            break;
        }
        case LARGE_TUPLE_EXT: {
            const uint32_t arity = get<tuple_t>().size();
            detail::copy_bytes<N>(&arity, i + 1, 4, output);
            encode_sub<N>(output, i + 5);
            break;
        }
        case LIST_EXT: {
            const uint32_t num_elems = get<list_t>().size();
            detail::copy_bytes<N>(&num_elems, i + 1, 4, output);
            encode_sub<N>(output, i + 5);
            output[i + encoded_size] = NIL_EXT;
            break;
        }
        default:
            throw std::runtime_error("Invalid eterm");
        }
    }
};



/* main.cpp */

#include <memory>
#include <thread>
#include <array>
#include <string>
#include <exception>

#include "common.hpp"
#include "erl_comm.hpp"
#include "physics.hpp"

// Bernstein's string hash function
// The constants are still a mystery
constexpr uint hash(const char* s)
{
    return *s ? static_cast<uint>(*s) ^ (33 * hash(s + 1)): 5381;
}

void decode_task(Physics& p, uint tidh, const eterm& argt) {
    util::no_init n;
    Job j = Job(n);
    switch(tidh) {
    case hash("add_char"): {
        // {'add_char', {id}}
        const uint& id = argt.get<eterm::tuple_t>()[0].get<eterm::integer_t>();

        j = Job(Task<TaskID::ADD_CHAR>{id});
        break;
    }
    case hash("rm_char"): {
        // {'rm_char', {id}}
        const uint& id = argt.get<eterm::tuple_t>()[0].get<eterm::integer_t>();

        j = Job(Task<TaskID::RM_CHAR>{id});
        break;
    }
    case hash("move_char"): {
        // {'move_char', {id, 'up'|'left'|'down'|'right', 'start'|'stop'}
        const eterm::tuple_t& tup = argt.get<eterm::tuple_t>();
        const uint& id = tup[0].get<eterm::integer_t>();
        const std::string& dirt = tup[1].get<eterm::atom_t>();
        const std::string& ont = tup[2].get<eterm::atom_t>();

        uint onh = hash(ont.c_str());
        bool on = onh == hash("start") ? true : false;
        uint dirh = hash(dirt.c_str());

        MovementState dir;
        switch (dirh) {
        case hash("up"):
            dir = MovementState::UP;
            break;
        case hash("left"):
            dir = MovementState::LEFT;
            break;
        case hash("down"):
            dir = MovementState::DOWN;
            break;
        case hash("right"):
            dir = MovementState::RIGHT;
            break;
        default:
            dir = MovementState::WAT;
            break;
        }

        j = Job(Task<TaskID::MOVE_CHAR>{id, dir, on});
        break;
    }
    case hash("orient_char"): {
        // {'orient_char', {id, {dirx, diry, dirz}}
        const eterm::tuple_t& tup = argt.get<eterm::tuple_t>();

        const uint& id = tup[0].get<eterm::integer_t>();
        const eterm::tuple_t& dirt = tup[1].get<eterm::tuple_t>();

        const double& dirx = dirt[0].get<double>();
        const double& diry = dirt[1].get<double>();
        const double& dirz = dirt[2].get<double>();

        j = Job(Task<TaskID::ORIENT_CHAR>{id, dirx, diry, dirz});
        break;
    }
    case hash("jump_char"): {
        // {'jump_char', {id}}
        const uint& id = argt.get<eterm::tuple_t>()[0].get<eterm::integer_t>();
        j = Job(Task<TaskID::JUMP_CHAR>{id});
        break;
    }
    case hash("foo"): {
        // {'foo', {foon}}
        const int& foon = argt.get<eterm::tuple_t>()[0].get<eterm::integer_t>();
        j = Job(Task<TaskID::FOO>{foon});
        break;
    }
    default:
        return;
    }
    p.post_job(std::move(j));
}

int main()
{
    std::ios::sync_with_stdio(false);

    std::array<byte, 65535> buf;

    bool stopped = false;

    Physics p = Physics();

    std::thread t = p.start_server();

    while (erlio::read_cmd(buf.data()) && !stopped) {
        eterm tup;
        try {
            tup = eterm::parse(buf);
        } catch (std::exception& e) {
            DPRINT(e.what(), ", Invalid buffer");
            continue;
        }
        if (tup.get_type() != TUPLE_EXT) {
            DPRINT("Not tuple type: ", tup);
            continue;
        }
        const eterm::tuple_t& t_elems = tup.get<eterm::tuple_t>();
        if (t_elems.size() != 2) {
            DPRINT("Invalid tuple size: ", tup);
            continue;
        }
        if (t_elems[0].get_type() != ATOM_EXT) {
            DPRINT("Not a command: ", t_elems[0]);
            continue;
        }
        if (t_elems[1].get_type() != TUPLE_EXT) {
            DPRINT("Invalid arg: ", t_elems[1]);
            continue;
        }
        uint tidh = hash(t_elems[0].get<eterm::atom_t>().c_str());
        const eterm& argt = t_elems[1];

        if (tidh == hash("stop")) {
            stopped = true;
        } else {
            try{
                decode_task(p, tidh, argt);
            } catch (std::exception& e) {
                DPRINT(e.what(), ", Invalid arg: ", argt);
                continue;
            }
        }
    }
    p.stop_server();
    t.join();
    return 0;
}

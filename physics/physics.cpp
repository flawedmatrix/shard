/* physics.cpp */

#include "erl_comm.hpp"
#include "physics.hpp"

namespace std {
    template<typename T, typename ...Args>
    std::unique_ptr<T> make_unique( Args&& ...args )
    {
        return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
    }
}

typedef std::chrono::duration<float> float_seconds;

/** Constructor */
Physics::Physics()
{
    running = std::make_shared<bool>(false);
    jobQueue = std::make_shared<std::queue<Job>>();
    queueMutex = std::make_shared<std::mutex>();
}

/** Copy Constructor */
Physics::Physics(const Physics& other)
{
    running = other.running;
    jobQueue = other.jobQueue;
    queueMutex = other.queueMutex;
    // Specifically copy only thread communication objects.
}

/** Copy Assignment Operator */
Physics& Physics::operator=(Physics other)
{
    running = other.running;
    jobQueue = other.jobQueue;
    queueMutex = other.queueMutex;
    // Specifically copy only thread communication objects.
    return *this;
}

Physics::~Physics()
{
    if (dynamicsWorld) {
        for (const auto &s : rigidBodies) {
            dynamicsWorld->removeRigidBody(s.get());
        }
        for (const auto &c : aliveCharIDs) {
            rm_char(c);
        }
    }
}

bool Physics::terminate_condition()
{
    std::lock_guard<std::mutex> lock(*queueMutex);
    return !(*running) && jobQueue->empty();
}

void Physics::add_static_body(btCollisionShape* body, btVector3 pos)
{
    collisionShapes.push_back(std::unique_ptr<btCollisionShape>(body));

    motionStates.push_back(std::make_unique<btDefaultMotionState>(
        btTransform(btQuaternion(0, 0, 0, 1), pos)
    ));

    btRigidBody::btRigidBodyConstructionInfo rigidBodyCI(
        0, motionStates.back().get(),
        collisionShapes.back().get(),
        btVector3(0, 0, 0)
    );

    rigidBodies.push_back(std::make_unique<btRigidBody>(rigidBodyCI));
    rigidBodies.back()->setRestitution(0);
    rigidBodies.back()->setLinearFactor(btVector3(0, 0, 0));
    rigidBodies.back()->setAngularFactor(btVector3(0, 0, 0));
    dynamicsWorld->addRigidBody(rigidBodies.back().get());
}

void Physics::setup_environment()
{
    // Initialize the standard components of a dynamics environment
    broadphase = std::make_unique<btDbvtBroadphase>();
    collisionConfiguration = std::make_unique<btDefaultCollisionConfiguration>();
    dispatcher = std::make_unique<btCollisionDispatcher>(
        collisionConfiguration.get()
    );
    solver = std::make_unique<btSequentialImpulseConstraintSolver>();
    dynamicsWorld = std::make_unique<btDiscreteDynamicsWorld>(
        dispatcher.get(),
        broadphase.get(),
        solver.get(),
        collisionConfiguration.get()
    );

    dynamicsWorld->setGravity(btVector3(0, 0, -10));
    dynamicsWorld->getDispatchInfo().m_allowedCcdPenetration = 0.0000001;

    // Ground shape
    add_static_body(
        new btStaticPlaneShape(btVector3(0, 0, 1), 1), btVector3(0, 0, -1)
    );
    // Center box
    add_static_body(
        new btBoxShape(btVector3(25, 25, 5)), btVector3(250, 250, 0)
    );
    // Walls
    add_static_body(
        new btBoxShape(btVector3(500, 2, 10)), btVector3(250, 501, 5)
    );
    add_static_body(
        new btBoxShape(btVector3(2, 500, 10)), btVector3(-1, 250, 5)
    );
    add_static_body(
        new btBoxShape(btVector3(500, 2, 10)), btVector3(250, -1, 5)
    );
    add_static_body(
        new btBoxShape(btVector3(2, 500, 10)), btVector3(501, 250, 5)
    );

    // Fall shape
    // collisionShapes.push_back(std::unique_ptr<btCollisionShape>(
    //     new btSphereShape(1)
    // ));

    // motionStates.push_back(std::make_unique<btDefaultMotionState>(
    //     btTransform(btQuaternion(0, 0, 0, 1), btVector3(250, 250, 50))
    // ));

    // btScalar mass = 1;
    // btVector3 fallInertia(0, 0, 0);
    // collisionShapes.back()->calculateLocalInertia(mass, fallInertia);

    // btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(
    //     mass, motionStates.back().get(),
    //     collisionShapes.back().get(), fallInertia
    // );

    // rigidBodies.push_back(std::make_unique<btRigidBody>(fallRigidBodyCI));
    // rigidBodies.back()->setCcdMotionThreshold(0.2f);
    // dynamicsWorld->addRigidBody(rigidBodies.back().get());

    ghostPairCallback = std::make_unique<btGhostPairCallback>();
    broadphase->getOverlappingPairCache()->setInternalGhostPairCallback(
        ghostPairCallback.get()
    );
}

bool Physics::add_char(uint id)
{
    if (!dynamicsWorld) { return false; }
    if (aliveCharIDs.find(id) != aliveCharIDs.end()) { return false; }
    aliveCharIDs.insert(id);

    // Character controller
    btTransform startTransform;
    startTransform.setIdentity();
    startTransform.setOrigin(btVector3(250, 250, 50));

    charGhosts[id] = std::make_unique<btPairCachingGhostObject>();
    charGhosts[id]->setWorldTransform(startTransform);

    btScalar characterHeight = 3;
    btScalar characterWidth = 2;

    charShapes[id] = std::unique_ptr<btConvexShape>(
        new btCapsuleShapeZ(characterWidth, characterHeight)
    );

    charGhosts[id]->setCollisionShape(charShapes[id].get());
    charGhosts[id]->setCollisionFlags(btCollisionObject::CF_CHARACTER_OBJECT);

    btScalar stepHeight = btScalar(0.35);
    int upAxis = 2; // Z axis is up
    charControllers[id] = std::make_unique<btKinematicCharacterController>(
        charGhosts[id].get(), charShapes[id].get(), stepHeight, upAxis
    );

    dynamicsWorld->addCollisionObject(
        charGhosts[id].get(),
        btBroadphaseProxy::CharacterFilter,
        btBroadphaseProxy::StaticFilter|btBroadphaseProxy::DefaultFilter
    );

    dynamicsWorld->addAction(charControllers[id].get());
    return true;
}

void Physics::move_char(uint id, MovementState dir, bool on)
{
    if (!dynamicsWorld) { return; }
    if (aliveCharIDs.find(id) == aliveCharIDs.end()) { return; }
    if (on) {
        charStates[id].movementState |= static_cast<byte>(dir);
    } else {
        charStates[id].movementState &= ~static_cast<byte>(dir);
    }
}

void Physics::orient_char(uint id, btVector3 dir)
{
    if (!dynamicsWorld) { return; }
    if (aliveCharIDs.find(id) == aliveCharIDs.end()) { return; }

    btVector3 front = btVector3(0, 1, 0);
    dir.normalize();
    btScalar dot = front.dot(dir);
    btQuaternion quat;
    if (dot < -0.99999) {
        btVector3 up = btVector3(0, 0, 1);
        quat.setRotation(up, SIMD_PI);
    } else if (dot > 0.99999) {
        quat = btQuaternion(0, 0, 0, 1);
    } else {
        btVector3 cross = dir.cross(front);
        quat = btQuaternion(cross.x(), cross.y(), cross.z(), 1 + dot);
        quat.normalize();
    }
    charStates[id].facingDirection = quat;
}

void Physics::jump_char(uint id)
{
    if (!dynamicsWorld) { return; }
    if (aliveCharIDs.find(id) == aliveCharIDs.end()) { return; }

    charStates[id].jump = true;
}

void Physics::update_dir_char(uint id)
{
    const byte& dir = charStates[id].movementState;
    const btQuaternion& facing = charStates[id].facingDirection;

    btTransform trans = charGhosts[id]->getWorldTransform();
    trans.setRotation(facing);
    charGhosts[id]->setWorldTransform(trans);

    btVector3 upDir = trans.getBasis()[2];
    btVector3 forwardDir = trans.getBasis()[1];
    btVector3 strafeDir = trans.getBasis()[0];
    upDir.normalize();
    forwardDir.normalize();
    strafeDir.normalize();

    btVector3 walkDirection = btVector3(0.0, 0.0, 0.0);

    if (dir & MovementState::UP) {
        walkDirection += forwardDir;
    }

    if (dir & MovementState::LEFT) {
        walkDirection -= strafeDir;
    }

    if (dir & MovementState::DOWN) {
        walkDirection -= forwardDir;
    }

    if (dir & MovementState::RIGHT) {
        walkDirection += strafeDir;
    }

    if (walkDirection.fuzzyZero()) {
        walkDirection = btVector3(0.0, 0.0, 0.0);
    } else {
        walkDirection.normalize();
    }
    charStates[id].walkDirection = walkDirection;
}

void Physics::rm_char(uint id)
{
    if (!dynamicsWorld) { return; }
    if (aliveCharIDs.find(id) != aliveCharIDs.end()) {
        dynamicsWorld->removeCollisionObject(charGhosts[id].get());
        dynamicsWorld->removeAction(charControllers[id].get());
        charGhosts.erase(id);
        charShapes.erase(id);
        charControllers.erase(id);
        aliveCharIDs.erase(id);
    }
}

TimePoint Physics::step_simulation(TimePoint last)
{
    if (!dynamicsWorld) { return std::chrono::high_resolution_clock::now(); }
    for (const auto &id : aliveCharIDs) {
        update_dir_char(id);
        charControllers[id]->setWalkDirection(charStates[id].walkDirection);

        if (charStates[id].jump) {
            charControllers[id]->jump();
        }
        charStates[id].jump = false;

        btTransform trans = charGhosts[id]->getWorldTransform();

        // Write the char position to erlang
        btVector3 pos = trans.getOrigin();

        std::vector<eterm> ea = {
            eterm(pos.getX()), eterm(pos.getY()), eterm(pos.getZ())
        };
        std::vector<eterm> fea = {
            eterm(ATOM_EXT, "char_pos"),
            eterm(static_cast<eterm::integer_t>(id)),
            eterm(TUPLE_EXT, ea)
        };

        eterm feat = eterm(TUPLE_EXT, fea);

        erlio::write_term(buf, feat);
    }
    auto now = std::chrono::high_resolution_clock::now();
    auto d = std::chrono::duration_cast<float_seconds>(now - last);

    dynamicsWorld->stepSimulation(d.count(), 10, 0.033f);

    return now;
}

void Physics::run_simulation()
{
    int i = 0;
    setup_environment();
    auto last = std::chrono::high_resolution_clock::now();
    do {
        {
            std::lock_guard<std::mutex> lock(*queueMutex);
            while (!jobQueue->empty()) {
                Job j = jobQueue->front();
                jobQueue->pop();
                handle_job(j);
            }
        }
        last = step_simulation(last);
        i += 1;
        std::this_thread::sleep_for(std::chrono::milliseconds(33));
    } while (!terminate_condition());
}

void Physics::handle_job(const Job& j)
{
    if (!j.valid()) { return; }
    switch (j.get_tid()) {
    case TaskID::ADD_CHAR: {
        const auto& t = j.get<TaskID::ADD_CHAR>();
        add_char(t.id);
        break;
    }
    case TaskID::RM_CHAR: {
        const auto& t = j.get<TaskID::RM_CHAR>();
        rm_char(t.id);
        break;
    }
    case TaskID::MOVE_CHAR: {
        const auto& t = j.get<TaskID::MOVE_CHAR>();
        move_char(t.id, t.dir, t.on);
        break;
    }
    case TaskID::ORIENT_CHAR: {
        const auto& t = j.get<TaskID::ORIENT_CHAR>();
        orient_char(t.id, btVector3(t.dirx, t.diry, t.dirz));
        break;
    }
    case TaskID::JUMP_CHAR: {
        const auto& t = j.get<TaskID::JUMP_CHAR>();
        jump_char(t.id);
        break;
    }
    case TaskID::FOO: {
        const auto& t = j.get<TaskID::FOO>();
        eterm et = eterm(static_cast<eterm::integer_t>(t.data * 2));
        erlio::write_term(buf, et);
        break;
    }
    default:
        break;
    }
}

std::thread Physics::start_server()
{
    *running = true;
    return std::thread(&Physics::run_simulation, this);
}

void Physics::stop_server()
{
    *running = false;
}

void Physics::post_job(Job&& j)
{
    std::lock_guard<std::mutex> lock(*queueMutex);
    jobQueue->push(std::move(j));
}


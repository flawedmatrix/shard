/* physics.hpp */
#pragma once

#include <thread>
#include <queue>
#include <vector>
#include <array>
#include <mutex>
#include <memory>
#include <chrono>
#include <unordered_set>
#include <unordered_map>

#include <btBulletDynamicsCommon.h>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include <BulletDynamics/Character/btKinematicCharacterController.h>
#include "common.hpp"

struct CharState {
    byte movementState = 0;
    btVector3 walkDirection = btVector3(0, 0, 0);
    btQuaternion facingDirection = btQuaternion(0, 0, 0, 1);
    bool jump = false;
};

typedef std::chrono::time_point<std::chrono::high_resolution_clock> TimePoint;

class Physics {
    // Thread communication objects
    std::shared_ptr<std::queue<Job>> jobQueue;
    std::shared_ptr<std::mutex> queueMutex;
    std::shared_ptr<bool> running;

    std::array<byte, 65535> buf;

    // Simulation pointers
    std::unique_ptr<btDbvtBroadphase> broadphase;
    std::unique_ptr<btDefaultCollisionConfiguration> collisionConfiguration;
    std::unique_ptr<btCollisionDispatcher> dispatcher;
    std::unique_ptr<btSequentialImpulseConstraintSolver> solver;
    std::unique_ptr<btDiscreteDynamicsWorld> dynamicsWorld;

    // Vectors of environment objects
    std::vector<std::unique_ptr<btCollisionShape>> collisionShapes;
    // We need this to keep motion states alive
    std::vector<std::unique_ptr<btDefaultMotionState>> motionStates;
    std::vector<std::unique_ptr<btRigidBody>> rigidBodies;

    // Maps of characters
    std::unordered_set<uint> aliveCharIDs;
    std::unordered_map<uint, CharState> charStates;
    std::unordered_map<uint, std::unique_ptr<btPairCachingGhostObject>>
        charGhosts;
    std::unordered_map<uint, std::unique_ptr<btConvexShape>> charShapes;
    std::unordered_map<uint, std::unique_ptr<btKinematicCharacterController>>
        charControllers;

    std::unique_ptr<btGhostPairCallback> ghostPairCallback;

    bool add_char(uint id);
    void move_char(uint id, MovementState dir, bool on);
    void orient_char(uint id, btVector3 dir);
    void update_dir_char(uint id);
    void jump_char(uint id);
    void rm_char(uint id);

    void add_static_body(btCollisionShape* body, btVector3 pos);
    void setup_environment();
    TimePoint step_simulation(TimePoint last);
    void run_simulation();

    void handle_job(const Job& j);
    bool terminate_condition();

public:
    /** Constructor */
    Physics();
    /** Copy Constructor */
    Physics(const Physics& other);
    /** Copy Assignment Operator */
    Physics& operator=(Physics other);
    /** Destructor */
    ~Physics();

    std::thread start_server();
    void stop_server();

    void post_job(Job&& j);
};

-- A solution contains projects, and defines the available configurations
solution "PhysicsServer"
    configurations { "Debug", "Release" }
    language "C++"
    targetdir "../priv"
    includedirs {
        "deps/bullet3/src/"
    }

    defines { "BT_USE_DOUBLE_PRECISION" }

    configuration "Debug"
        flags { "StaticRuntime", "EnableSSE2", "FloatFast", "Symbols" }

    configuration "Release"
        flags { "StaticRuntime", "EnableSSE2", "FloatFast", "Optimize" }

    configuration "gmake"
        -- Enables C++11 support
        buildoptions { "-std=c++11" }

    configuration "macosx"
        -- Enables C++11 support
        buildoptions { "-stdlib=libc++ -std=c++11" }
        links { "c++" }

    -- A project defines one build target
    project "PhysicsServer"
        kind "ConsoleApp"
        files { "*.cpp", "*.hpp" }
        targetname "physerv"
        links { "BulletDynamics", "BulletCollision", "LinearMath" }

        configuration "gmake"
            links { "pthread" }

        configuration "Debug"
            defines { "DEBUG" }
            flags { "ExtraWarnings", "FatalWarnings" }

        configuration "Release"
            defines { "NDEBUG" }
            flags { "ExtraWarnings", "FatalWarnings" }

    project "BulletDynamics"
        kind "StaticLib"
        targetdir "lib"
        files {
            "deps/bullet3/src/BulletDynamics/**.cpp",
            "deps/bullet3/src/BulletDynamics/**.h"
        }

    project "BulletCollision"
        kind "StaticLib"
        targetdir "lib"
        files {
            "deps/bullet3/src/BulletCollision/**.cpp",
            "deps/bullet3/src/BulletCollision/**.h"
        }

    project "LinearMath"
        kind "StaticLib"
        targetdir "lib"
        files {
            "deps/bullet3/src/LinearMath/**.cpp",
            "deps/bullet3/src/LinearMath/**.h"
        }


/* variant.hpp */
#pragma once

#include <utility>
#include <typeinfo>
#include <type_traits>
#include <algorithm> // std::move/swap
#include <stdexcept> // runtime_error
#include <new> // operator new
#include <cstddef> // size_t
#include <iosfwd>
#include <string>

#ifdef _MSC_VER
    // http://msdn.microsoft.com/en-us/library/z8y1yy88.aspx
    #ifdef NDEBUG
        #define VARIANT_INLINE __forceinline
    #else
        #define VARIANT_INLINE __declspec(noinline)
    #endif
#else
    #ifdef NDEBUG
        #define VARIANT_INLINE inline __attribute__((always_inline))
    #else
        #define VARIANT_INLINE __attribute__((noinline))
    #endif
#endif

namespace util { namespace detail {

static constexpr std::size_t invalid_value = std::size_t(-1);

template <typename T, typename...Types>
struct direct_type;

template <typename T, typename First, typename...Types>
struct direct_type<T, First, Types...> {
    static constexpr std::size_t index = std::is_same<T, First>::value ?
        sizeof...(Types) :
        direct_type<T, Types...>::index;
};

template <typename T>
struct direct_type<T> {
    static constexpr std::size_t index = invalid_value;
};

template <typename T, typename...Types>
struct convertible_type;

template <typename T, typename First, typename...Types>
struct convertible_type<T, First, Types...> {
    static constexpr std::size_t index = std::is_convertible<T, First>::value ?
        sizeof...(Types) :
        convertible_type<T, Types...>::index;
};

template <typename T>
struct convertible_type<T> {
    static constexpr std::size_t index = invalid_value;
};

template <typename T, typename...Types>
struct value_traits {
    static constexpr std::size_t direct_index = direct_type<T, Types...>::index;
    static constexpr std::size_t index = (direct_index == invalid_value) ?
            convertible_type<T, Types...>::index :
            direct_index;
};

template <typename T, typename...Types>
struct is_valid_type;

template <typename T, typename First, typename... Types>
struct is_valid_type<T, First, Types...> {
    static constexpr bool value = std::is_convertible<T, First>::value
        || is_valid_type<T, Types...>::value;
};

template <typename T>
struct is_valid_type<T> : std::false_type {};

template <std::size_t N, typename ... Types>
struct select_type {
    static_assert(N < sizeof...(Types), "index out of bounds");
};

template <std::size_t N, typename T, typename ... Types>
struct select_type<N, T, Types...> {
    using type = typename select_type<N - 1, Types...>::type;
};

template <typename T, typename ... Types>
struct select_type<0, T, Types...> {
    using type = T;
};

} // namespace detail

// static visitor
template <typename R = void>
struct static_visitor {
    using result_type = R;
protected:
    static_visitor() {}
    ~static_visitor() {}
};

template <std::size_t arg1, std::size_t ... others>
struct static_max;

template <std::size_t arg>
struct static_max<arg> {
    static const std::size_t value = arg;
};

template <std::size_t arg1, std::size_t arg2, std::size_t ... others>
struct static_max<arg1, arg2, others...> {
    static const std::size_t value = arg1 >= arg2 ?
        static_max<arg1, others...>::value :
        static_max<arg2, others...>::value;
};

template<typename... Types>
struct variant_helper;

template<typename T, typename... Types>
struct variant_helper<T, Types...> {
    VARIANT_INLINE static void destroy(const std::size_t id, void* data)
    {
        if (id == sizeof...(Types)) {
            reinterpret_cast<T*>(data)->~T();
        } else {
            variant_helper<Types...>::destroy(id, data);
        }
    }

    VARIANT_INLINE static void move(
        const std::size_t old_id, void* old_value, void* new_value
    )
    {
        if (old_id == sizeof...(Types)) {
            new (new_value) T(std::move(*reinterpret_cast<T*>(old_value)));
        } else {
            variant_helper<Types...>::move(old_id, old_value, new_value);
        }
    }

    VARIANT_INLINE static void copy(
        const std::size_t old_id, const void* old_value, void* new_value
    )
    {
        if (old_id == sizeof...(Types)) {
            new (new_value) T(*reinterpret_cast<const T*>(old_value));
        } else {
            variant_helper<Types...>::copy(old_id, old_value, new_value);
        }
    }
};

template<> struct variant_helper<> {
    VARIANT_INLINE static void destroy(const std::size_t, void*) {}
    VARIANT_INLINE static void move(const std::size_t, void*, void*) {}
    VARIANT_INLINE static void copy(const std::size_t, const void*, void*) {}
};

struct no_init {};

template<typename... Types>
class variant
{
private:

    static const std::size_t data_size = static_max<sizeof(Types)...>::value;
    static const std::size_t data_align = static_max<alignof(Types)...>::value;

    using data_type = typename std::aligned_storage<data_size, data_align>::type;
    using helper_type = variant_helper<Types...>;

    std::size_t type_index;
    data_type data;

public:


    VARIANT_INLINE variant():
        type_index(sizeof...(Types) - 1)
    {
        new (&data) typename detail::select_type<0, Types...>::type();
    }

    VARIANT_INLINE variant(no_init):
        type_index(detail::invalid_value) {}

    template <typename T, class = typename std::enable_if<
                         detail::is_valid_type<T, Types...>::value>::type>
    VARIANT_INLINE explicit variant(T const& val) noexcept :
        type_index(detail::value_traits<T, Types...>::index)
    {
        constexpr std::size_t index =
            sizeof...(Types) - detail::value_traits<T, Types...>::index - 1;
        using target_type = typename detail::select_type<index, Types...>::type;
        new (&data) target_type(val);
    }

    template <typename T, class = typename std::enable_if<
                          detail::is_valid_type<T, Types...>::value>::type>
    VARIANT_INLINE variant(T && val) noexcept :
        type_index(detail::value_traits<T, Types...>::index)
    {
        constexpr std::size_t index =
            sizeof...(Types) - detail::value_traits<T, Types...>::index - 1;
        using target_type = typename detail::select_type<index, Types...>::type;
        new (&data) target_type(std::forward<T>(val)); // nothrow
    }

    VARIANT_INLINE variant(variant<Types...> const& old):
        type_index(old.type_index)
    {
        helper_type::copy(old.type_index, &old.data, &data);
    }

    VARIANT_INLINE variant(variant<Types...>&& old) noexcept :
        type_index(old.type_index)
    {
        helper_type::move(old.type_index, &old.data, &data);
    }

    friend void swap(variant<Types...> & first, variant<Types...> & second)
    {
        using std::swap; //enable ADL
        swap(first.type_index, second.type_index);
        swap(first.data, second.data);
    }

    VARIANT_INLINE variant<Types...>& operator=(variant<Types...> other)
    {
        swap(*this, other);
        return *this;
    }

    // conversions
    // move-assign
    template <typename T>
    VARIANT_INLINE variant<Types...>& operator=(T && rhs) noexcept
    {
        variant<Types...> temp(std::move(rhs));
        swap(*this, temp);
        return *this;
    }

    // copy-assign
    template <typename T>
    VARIANT_INLINE variant<Types...>& operator=(T const& rhs)
    {
        variant<Types...> temp(rhs);
        swap(*this, temp);
        return *this;
    }

    template<typename T>
    VARIANT_INLINE bool is() const
    {
        return (type_index == detail::direct_type<T, Types...>::index);
    }

    VARIANT_INLINE bool valid() const
    {
        return (type_index != detail::invalid_value);
    }

    template<typename T, typename... Args>
    VARIANT_INLINE void set(Args&&... args)
    {
        helper_type::destroy(type_index, &data);
        new (&data) T(std::forward<Args>(args)...);
        type_index = detail::direct_type<T, Types...>::index;
    }

    template<typename T>
    VARIANT_INLINE T& get()
    {
        if (type_index == detail::direct_type<T, Types...>::index) {
            return *reinterpret_cast<T*>(&data);
        } else {
            throw std::runtime_error("in get()");
        }
    }

    template<typename T>
    VARIANT_INLINE T const& get() const
    {
        if (type_index == detail::direct_type<T, Types...>::index) {
            return *reinterpret_cast<T const*>(&data);
        } else {
            throw std::runtime_error("in get()");
        }
    }

    VARIANT_INLINE std::size_t get_type_index() const
    {
        return type_index;
    }

    ~variant() noexcept
    {
        helper_type::destroy(type_index, &data);
    }

};

}


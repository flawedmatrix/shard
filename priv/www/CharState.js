var CharState = function(pos) {
    if (!defined(pos)) {
        this.pos = vector(250, 250, 250);
    } else {
        this.pos = pos;
    }
    this.orientation = vector(0, 1, 0);
    this.speed = 1.0;

    this.renderPos = this.pos.clone();
    this.renderOrient = this.orientation.clone();

    this.stateBuffer = [];
    this.state0 = undefined;
    this.state1 = undefined;

    this.servState = undefined;
    this.deltasBuffer = [];
    this.gravity = 0.2;
    this.og_vel = vector(0, 0, 0);
    this.jumpCommand = false;
};

CharState.prototype.setPhysicsHandler = function(ep) {
    // newDir = enforcePhysics(origPos, origVel);
    this.enforcePhysics = ep;
};

CharState.prototype.setGroundCheck = function(gc) {
    // onGround = groundCheck(position);
    this.groundCheck = gc;
};

CharState.prototype.moveDir = function(keys) {
    var facing = this.orientation;
    var direction = vector(0, 0, 0);
    var up = keys.up;
    var left = keys.left;
    var down = keys.down;
    var right = keys.right;
    if (up != down) { if (up) { direction.add(facing);
        }
        if (down) {
            direction.add(vector(-facing.x, -facing.y, 0));
        }
    }
    if (left != right) {
        if (left) {
            direction.add(vector(-facing.y, facing.x, 0));
        }
        if (right) {
            direction.add(vector(facing.y, -facing.x, 0));
        }
    }
    direction.normalize();
    return direction;
};

CharState.prototype.jump = function(keys) {
    if (this.groundCheck(this.pos)) {
        this.og_vel.copy(this.moveDir(keys));
        this.og_vel.multiplyScalar(this.speed);
        this.og_vel.setZ(1.0);
        this.jumpCommand = true;
    }
};

CharState.prototype.moveStep = function(keys, update_time) {
    this.speed = this.servState.speed;
    var velocity = vector(0, 0, 0);
    velocity.copy(this.moveDir(keys));
    velocity.multiplyScalar(this.speed);
    if (this.jumpCommand || !this.groundCheck(this.pos)) {
        this.jumpCommand = false;
        this.og_vel.setZ(this.og_vel.z - this.gravity);
        var dv = this.enforcePhysics(this.pos, this.og_vel);
    } else {
        var dv = this.enforcePhysics(this.pos, velocity);
        console.log("On the ground");
    }
    this.og_vel.copy(dv);
    if (!isVectorZero(dv)) {
        this.deltasBuffer.push({
            "timestamp": update_time,
            "velocity": dv
        });
        this.pos.add(dv);
    }
    this.inputPrediction();
    var snapshot = new Entity();
    snapshot.pos = this.pos.clone();
    snapshot.orientation = this.orientation.clone();
    snapshot.timestamp = update_time;
    this.stateBuffer.push(snapshot);
};

// CharState.prototype.enforcePhysics = function(velocity) {
//     var newPos = this.pos.clone();
//     newPos.add(velocity);
//     var unclampedPos = newPos.clone();
//     newPos.clamp(vector(0), vector(500.0));
//     var diff = vector(0);
//     diff.subVectors(newPos, unclampedPos);
//     var dv = velocity.clone();
//     dv.add(diff);
//     return dv;
// };

CharState.prototype.inputPrediction = function() {
    if (defined(this.servState)) {
        var earliest = this.deltasBuffer[0];
        var lastTime = this.servState.timestamp;
        while (defined(earliest) && (earliest.timestamp < lastTime)) {
            this.deltasBuffer.shift();
            earliest = this.deltasBuffer[0];
        }
        var p_servPos = this.servState.pos.clone();
        for (var i = 0; i < this.deltasBuffer.length; i += 1) {
            var velocity = this.deltasBuffer[i].velocity;
            p_servPos.add(velocity);
        }
        if (!defined(this.pserv)) {
            this.pserv = new Entity();
        }
        var snapshot = new Entity();
        this.pserv.pos = p_servPos;
        this.pserv.orientation = this.orientation;
        // TODO: Find a reasonable epsilon and alpha value
        var dist = p_servPos.distanceToSquared(this.pos);
        // if (dist > 5000) {
        //     this.pos.copy(p_servPos);
        // } else {
        //     var alpha = 0.50;
        //     var prevz = this.pos.z;
        //     this.pos.lerp(p_servPos, alpha);
        //     this.pos.setZ(prevz);
        //     this.pos.clamp(vector(0.0), vector(500.0));
        // }
    }
};

CharState.prototype.orient = function(direction) {
    this.orientation = direction.clone();
    this.orientation.normalize();
};

CharState.prototype.advanceRenderState = function() {
    if (defined(this.renderDelay)) {
        var delay = this.renderDelay;
    } else {
        var delay = game.LERP_DELAY;
    }
    var now = getTimestamp() - delay;
    var stateBuffer = this.stateBuffer;

    if (!defined(this.state1)) {
        if (stateBuffer.length > 0) {
            this.state1 = stateBuffer.shift();
        }
    }
    while ((stateBuffer.length > 0) && (now > this.state1.timestamp)) {
        this.state0 = this.state1;
        this.state1 = this.stateBuffer.shift();
    }
    if (defined(this.state1)) {
        if ((now > this.state1.timestamp) || (!defined(this.state0))) {
            this.renderPos = this.state1.pos.clone();
            this.renderOrient = this.state1.orientation.clone();
        } else {
            this.interpolate(now);
        }
    }
};

CharState.prototype.interpolate = function(now) {
    var state0 = this.state0;
    var state1 = this.state1;
    var ts_diff = state1.timestamp - state0.timestamp;
    if (ts_diff > 1.0e-10) {
        var int_loc = (now - state0.timestamp)/ts_diff;
        this.renderPos = state0.pos.clone();
        this.renderPos.lerp(state1.pos, int_loc);

        this.renderOrient = state0.orientation.clone();
        this.renderOrient.lerp(state1.orientation, int_loc);
        this.renderOrient.normalize();
    }
};


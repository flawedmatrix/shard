// Client code
var Client = function() {
    // Default client parameters
    this.TICKINT = 33; // Client tick interval
    this.LERP_DELAY = 66; // Interpolation interval
    this.UPDATEINT = 33;
};

Client.prototype.init = function() {
    this.ws = new WorldState();
    this.ih = new InputHandler(this.ws);
    this.ih.bindKeys();
};

var game = new Client();
game.init();

var Entity = function() {
    // Defaults should never be used
    this.id = undefined; // INTEGER
    this.pos = undefined; // VECTOR
    this.orientation = undefined; // VECTOR
    this.speed = undefined; // FLOAT
    this.timestamp = undefined; // TIMESTAMP (float)
};

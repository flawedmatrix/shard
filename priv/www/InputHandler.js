var InputHandler = function(worldState) {
    this.worldState = worldState;

    var canvas = $('#game-ui');
    this.WIDTH = 500;
    this.HEIGHT = 500;

    var offset = canvas.offset();
    this.offX = offset.left;
    this.offY = offset.top;

    $(document).mousedown(this.mouseDown.bind(this));
    $(document).mouseup(this.mouseUp.bind(this));
    $(document).mousemove(this.mouseMove.bind(this));
    $(document).bind('contextmenu', function(e) {
        return false;
    });
};

InputHandler.prototype.mouseToCanvas = function(cx, cy) {
    var x = cx - this.offX;
    var y = cy - this.offY;
    return vector(x, this.HEIGHT - y, 0);
};

InputHandler.prototype.sampleOrientation = function() {
    if (defined(this.lastMousePos)) {
        var diff = vector(0);
        diff.subVectors(this.mousePos, this.lastMousePos);
        this.worldState.rotateView(diff.x, diff.y);
    }
    this.lastMousePos = this.mousePos;
    this.dragTimer = window.setTimeout(function() {
        this.sampleOrientation();
    }.bind(this), 33);
};

InputHandler.prototype.mouseDown = function(event) {
    if (event.button == 2) {
        this.mousePos = vector(event.clientX, event.clientY, 0);
        this.sampleOrientation();
    }
    event.preventDefault();
};

InputHandler.prototype.mouseUp = function(event) {
    if (event.button == 2) {
        window.clearTimeout(this.dragTimer);
        this.lastMousePos = undefined;
    }
    event.preventDefault();
};

InputHandler.prototype.mouseMove = function(event) {
    this.mousePos = vector(event.clientX, event.clientY, 0);
};

InputHandler.prototype.bindKeys = function() {
    var self = this;

    var ws = self.worldState;

    var arrow_opts = { "preserve": true, "propagate": true };
    // Bind keyboard keys
    Combo.onEvent("w:up", function() { ws.move("up", false); }, arrow_opts);
    Combo.onEvent("w:down", function() { ws.move("up", true); }, arrow_opts);

    Combo.onEvent("a:up", function() { ws.move("left", false); }, arrow_opts);
    Combo.onEvent("a:down", function() { ws.move("left", true); }, arrow_opts);

    Combo.onEvent("s:up", function() { ws.move("down", false); }, arrow_opts);
    Combo.onEvent("s:down", function() { ws.move("down", true); }, arrow_opts);

    Combo.onEvent("d:up", function() { ws.move("right", false); }, arrow_opts);
    Combo.onEvent("d:down", function() { ws.move("right", true); }, arrow_opts);

    Combo.on("space", function() { ws.jump(); });

    Combo.on("z", function() { ws.action1(); });
    Combo.on("x", function() { ws.action2(); });

    Combo.on("f", function() { ws.sendChat(); });

    Combo.on("g", function() { ws.useItem(0); });
    Combo.on("h", function() { ws.useItem(100); });
    Combo.on("j", function() { ws.useItem(8064); });
    Combo.on("k", function() { ws.useItem(65000); });

};

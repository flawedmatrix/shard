// Constants

// For now, the opcode length is 8
var OPCODE_LENGTH = 8;

var OPH = {
    MOVEMENT : 0,
    ACTION1  : 1,
    ACTION2  : 2,
    ITEM_USE : 3,
    ORIENT   : 5,
    PING     : 6,
    CHAT     : 7,
    JUMP     : 8
};

var IPH = {
    STATE   : 0,
    ACK     : 4,
    PONG    : 6,
    CHAT    : 7
};

var SF = {
    ID          : 0,
    TICKINT     : 1,
    TIME        : 2,
    POS         : 101,
    ORIENT      : 102,
    VEL         : 103
};

var directions = {
    up    : 0,
    left  : 1,
    down  : 2,
    right : 3
};

var LATENCY = 12;

// Static Functions
function packVector(v) {
    // Packing the vector
    var xSign = (v.x < 0);
    var ySign = (v.y < 0);
    var zSign = (v.z < 0);
    var x = Math.abs(v.x);
    var y = Math.abs(v.y);
    var z = Math.abs(v.z);
    var w = 126.0 / (x + y + z);
    // Make sure 0 <= XBits < 127
    x = Math.floor(x * w);
    // Make sure 0 <= YBits < 127
    y = Math.floor(y * w);
    if (y >= 64) {
        x = 127 - x;
        y = 127 - y;
    }
    return {
        xSign: xSign,
        ySign: ySign,
        zSign: zSign,
        x: x,
        y: y
    };
}

function unpackVector(p) {
    // If we directly do a reverse transform, we'll get a
    // Point on the plane (0,0), (0, 126), (126, 0)
    // We need a point on the unit sphere, so we need to
    // normalize the point so that x^2 + y^2 + z^2 = 1
    var xSign = p.xSign;
    var ySign = p.ySign;
    var zSign = p.zSign;
    var x = p.x;
    var y = p.y;
    if ((x + y) >= 127) {
        x = 127 - x;
        y = 127 - y;
    }
    var z = 127-x-y;
    var n = vector(x, y, 127-x-y);
    n.normalize();
    x = n.x;
    y = n.y;
    z = n.z;
    if (xSign) { x = -x; }
    if (ySign) { y = -y; }
    if (zSign) { z = -z; }
    return vector(x, y, z);
}

function wsUrl() {
    var l = window.location;
    var uri;
    if (l.protocol === "https:") {
        uri = "wss://";
    } else {
        uri = "ws://";
    }
    uri += l.hostname;
    if ((l.port != 80) && (l.port != 443)) {
        uri += ":" + l.port;
    }
    uri += "/websocket";
    return uri;
}

var MessageHandler = function() {
    var self = this;

    if (!("WebSocket" in window)) {
        alert("This browser does not support WebSockets");
        return;
    }
    /* @TODO: Change to your own server IP address */
    self.ws = new WebSocket(wsUrl());
    // Receive arraybuffers instead of blobs
    self.ws.binaryType = "arraybuffer";

    // Bind WebSocket Handlers
    self.ws.onopen      = self.onSocketOpen.bind(self);
    self.ws.onmessage   = self.onSocketMsg.bind(self);
    self.ws.onclose     = self.onSocketClose.bind(self);

    this.rtt = 20;

    this.lastUpdate = 0;
    this.updateInt = 33;

    // Measuring the size of bursts
    this.numBursts = 0;
    this.accBurstSize = 0;

    this.suCount = 0;

    this.lastPing = getTimestamp();
};

MessageHandler.prototype.setSnapshotsFunc = function(addSnapshots) {
    this.addSnapshots = addSnapshots;
};

MessageHandler.prototype.setMainIdFunc = function(setMainId) {
    this.setMainId = setMainId;
};

MessageHandler.prototype.setTerminateFunc = function(terminate) {
    this.terminateClient = terminate;
};

// ------------------------------------------------------------------
// WebSocket Handlers
// ------------------------------------------------------------------

// Called when socket is opened
MessageHandler.prototype.onSocketOpen = function() {
    console.log('Connected');
    this.pongTime = getTimestamp();
    this.offsets = [];
    this.pingTimer = window.setTimeout(function() {
        this.sendPingPacket();
    }.bind(this), 10000);
};

// Called when data is received on the socket
MessageHandler.prototype.onSocketMsg = function(e) {
    var msg = e.data;
    //console.log("Received: ");
    //console.log(new Uint8Array(msg));
    // Introduce delay in message receiving
    // this.recvTimer = window.setTimeout(function() {
        this.messageHandler(msg);
    // }.bind(this), LATENCY);

};

// Called when the socket connection is closed
MessageHandler.prototype.onSocketClose = function() {
    console.log('Connection closed');
    // Clear all timers
    // window.clearTimeout(this.sendTimer);
    // window.clearTimeout(this.recvTimer);
    this.terminateClient();
    window.clearTimeout(this.pingTimer);
};

// Dispatches packets to their respective handlers
MessageHandler.prototype.messageHandler = function(data) {
    var buf = new Uint8Array(data);
    var opcode = buf[0];
    switch (opcode) {
        case IPH.STATE:
            var receiptTime = getTimestamp();
            if (defined(this.measuring)) {
                this.updateStreamMeasure(receiptTime);
            }
            this.decodeStatePacket(buf, receiptTime);
            break;
        case IPH.ACK:
            break;
        case IPH.PONG:
            this.handlePong(buf);
            break;
        case IPH.CHAT:
            console.log("Got chat packet");
            var parameters = buf[1];
            var usrLength = buf[2];
            var msgLoc = 3 + usrLength;
            var username = u8a2str(buf.subarray(3, msgLoc));
            var message = u8a2str(buf.subarray(msgLoc));
            console.log("Params: " + parameters);
            console.log("Message from " + username +": " + message);
            break;
        default:
            console.log("Got unknown packet");
            break;
    }
};

// General function to handle sending a payload for a specific opcode.
// The number of bytes to allocate for the packet must be specified.
// The payload must be an array where each element is a
// length 2 tuple in the form of [bits, data]
MessageHandler.prototype.sendPacket = function(opcode, bytes, payload) {
    var buffer = new Uint8Array(bytes);
    var bs = new BitStream(buffer);
    bs.writeBits(OPCODE_LENGTH, opcode);
    var i, el;
    for (i = 0; i < payload.length; i += 1) {
        el = payload[i];
        bs.writeBits(el[0], el[1]);
    }
    // this.sendTimer = window.setTimeout(function() {
        this.ws.send(buffer);
    // }.bind(this), LATENCY);
};

MessageHandler.prototype.sendMovePacket = function(direction, keyup) {
    var payload = [];
    // Set the next two bits to the last two bits of direction
    payload.push([2, directions[direction]]);
    // Set the keydown|keyup bit
    payload.push([1, 0+keyup]);
    this.sendPacket(OPH.MOVEMENT, 2, payload);
};

MessageHandler.prototype.sendOrientPacket = function(vector) {
    var p = packVector(vector);
    var payload = [];
    // Set x sign bit
    payload.push([1, p.xSign]);
    // Set x: 7
    payload.push([7, p.x]);
    // Set y sign bit
    payload.push([1, p.ySign]);
    // Set y: 6
    payload.push([6, p.y]);
    // Set z sign bit
    payload.push([1, p.zSign]);
    this.sendPacket(OPH.ORIENT, 3, payload);

};

MessageHandler.prototype.sendAction1Packet = function() {
    this.sendPacket(OPH.ACTION1, 1, []);
};

MessageHandler.prototype.sendAction2Packet = function() {
    this.sendPacket(OPH.ACTION2, 1, []);
};

MessageHandler.prototype.sendItemUsePacket = function(item) {
    // Send an item use packet where the payload is 16 bits for the item ID
    this.sendPacket(OPH.ITEM_USE, 3, [[16, item]]);
};

// The current implementation of this method does not use
// MessageHandler::sendPacket since it is more efficient
// To handle the chat packet in this manner
MessageHandler.prototype.sendChatPacket = function(txt) {
    // Append the opcode byte to the string
    var opcodeByte = String.fromCharCode(OPH.CHAT);
    var params = "\x00";
    var str = opcodeByte + params + txt;
    var buffer = str2u8a(str);

    // Specify any parameters here

    this.ws.send(buffer);
};

MessageHandler.prototype.sendJumpPacket = function() {
    this.sendPacket(OPH.JUMP, 1, []);
};

MessageHandler.prototype.scaleUpdateInt = function() {
    var newUpdateInt = this.updateInt;
    if (this.numBursts === 0) {
        // If there were no bursts after the update tick change,
        // That is a green flag to lower the update interval
        newUpdateInt = this.updateInt * 0.90;
    } else {
        var avgBurstSize = 1 + (this.accBurstSize / this.numBursts);
        if ((this.numBursts/this.suCount) > 0.05) {
            // If more than 5% of the state packets were late
            // It would be a good idea to scale the update interval
            // by the average size of the burst
            newUpdateInt = this.updateInt * avgBurstSize;
        } else {
            // Otherwise, if less than 5% of the packets were late
            // It would be a good idea to slowly lower the update
            // interval to lower this percentage.
            newUpdateInt = this.updateInt * 0.99;
        }
    }
    // Constrain the update interval to an int between 33 and 300
    return Math.floor(Math.max(33, Math.min(300, newUpdateInt)));
};

MessageHandler.prototype.sendPingPacket = function() {
    var offsetTotal = this.offsets.reduce(function(a, b) {
        return a + b;
    });
    var avgOffset = offsetTotal / this.offsets.length;
    var variance = 0;
    var maxDiff = 0;
    for (var i = 0; i < this.offsets.length; i++) {
        var diff = (this.offsets[i] - avgOffset);
        var absDiff = Math.abs(diff);
        if (absDiff > maxDiff) {
            maxDiff = absDiff;
        }
        variance += (diff*diff);
    };
    variance /= this.offsets.length;
    // console.log("Average offset " + avgOffset);
    // console.log("Standard deviation " + Math.sqrt(variance));
    // console.log("Number of bursts " + this.numBursts);
    // console.log("Acc burst size " + this.accBurstSize);
    // console.log("Number of su's received " + this.suCount);
    var reqUpdateInt = game.UPDATEINT;//this.scaleUpdateInt();
    // console.log("Requested update interval " + reqUpdateInt);
    var payload = [[16, reqUpdateInt]];
    this.lastPing = getTimestamp();
    this.sendPacket(OPH.PING, 3, payload);
};

MessageHandler.prototype.handlePong = function(buf) {
    var newRtt = getTimestamp() - this.lastPing;
    this.rtt = 0.875 * this.rtt + 0.125 * newRtt;
    this.updateInt = (new Uint16Array(buf.buffer.slice(1)))[0];
    // console.log("RTT " + this.rtt);
    // console.log("Approved update interval " + this.updateInt);
    game.LERP_DELAY = this.updateInt * 2;
    // Reset burst counters
    // Measurements in between the ping and the pong are discarded
    this.measuring = true;
    this.suCount = 0;
    this.burstMode = false;
    this.numBursts = 0;
    this.accBurstSize = 0;
    this.expTime = 0;
    this.totalVar = 0;

    this.pongTime = getTimestamp();
    this.offsets = [];
    this.pingTimer = window.setTimeout(function() {
        this.sendPingPacket();
    }.bind(this), 10000);
}

MessageHandler.prototype.updateStreamMeasure = function(receiptTime) {
    // Calculates connection quality based on the time between received
    // state updates
    this.suCount += 1;
    var timeDiff = receiptTime - this.lastUpdate;
    // console.log(timeDiff);
    if (timeDiff < this.updateInt * 0.75) {
        if (this.burstMode === false) {
            this.burstMode = true;
            this.numBursts += 1;
            // console.log("BURST START");
        }
        this.accBurstSize += 1;
    } else {
        // if (this.burstMode === true) {
        //     console.log("BURST END");
        // }
        this.burstMode = false;
    }
    this.lastUpdate = receiptTime;
};

MessageHandler.prototype.decodeStatePacket = function(buf, receiptTime) {
    var timestamp  = receiptTime - this.rtt;
    var ptr = 1;
    var rawFields = [];
    // Consume all fields until they are all properly decoded
    while (ptr < buf.length) {
        var fieldType = buf[ptr];
        var fieldLen = buf[ptr + 1];
        ptr += 2;
        var fieldBuf = buf.buffer.slice(ptr, ptr + fieldLen);
        ptr += fieldLen;
        rawFields.push(this.decodeField(fieldType, fieldBuf));
    }
    var idx = 0;
    // On the second pass, process all the client parameters if any
    // Exit loop if it encounters any id field
    while (idx < rawFields.length) {
        var type = rawFields[idx][0];
        var value = rawFields[idx][1];
        if (type === SF.ID) {
            id = value;
            this.setMainId(id);
            break;
        }
        this.handleParameters(type, value);
        idx += 1;
    }

    if (!defined(game.TIME)) {
        console.log("UNKNOWN TIME!");
    }
    var timeSincePong = receiptTime - this.pongTime;
    var offset = timeSincePong - game.TIME;
    this.offsets.push(offset);

    game.TIME = undefined;
    timestamp -= offset;

    // After the client parameters have been processed, load into entity
    // snapshots
    var snapshots = [];
    while (idx < rawFields.length) {
        var type = rawFields[idx][0];
        var value = rawFields[idx][1];
        if (type === SF.ID) {
            // If the field is an id, create the snapshot if it already
            // hasn't been created
            id = value;
            if (!snapshots.hasOwnProperty(id)) {
                // Create the entity snapshot if it already hasn't been created
                snapshots[id] = new Entity();
                snapshots[id].id = id;
                snapshots[id].timestamp = timestamp;
            }
        } else {
            this.handleField(snapshots[id], type, value);
        }
        idx += 1;
    }

    this.addSnapshots(snapshots);
};

MessageHandler.prototype.handleParameters = function(type, value) {
    switch(type) {
        case SF.TICKINT:
            game.TICKINT = value;
            break;
        case SF.TIME:
            game.TIME = value;
            break;
        default:
            break;
    }
};

MessageHandler.prototype.handleField = function(target, type, value) {
    switch (type) {
        case SF.POS:
            // Position
            target.pos = value;
            break;
        case SF.ORIENT:
            // Orientation
            target.orientation = value;
            break;
        case SF.VEL:
            // Velocity
            target.speed = value;
            break;
        default:
            break;
    }
};

MessageHandler.prototype.decodeField = function(type, fieldBuf) {
    switch (type) {
        case SF.ID:
            var buf = new Uint8Array(fieldBuf);
            var id = 0;
            for (var i = 0; i < buf.length; i += 1) {
                id += buf[i] << i;
            }
            return [SF.ID, id];
        case SF.TICKINT:
            var buf = new Uint8Array(fieldBuf);
            return [SF.TICKINT, buf[0]];
        case SF.TIME:
            var buf = new Uint16Array(fieldBuf);
            return [SF.TIME, buf[0]];
        case SF.RTT:
            var buf = new Float32Array(fieldBuf);
            return [SF.RTT, buf[0]];
        case SF.POS:
            // Position
            var buf = new Float32Array(fieldBuf);
            var x = buf[0];
            var y = buf[1];
            var z = buf[2];
            return [SF.POS, vector(x, y, z)];
        case SF.ORIENT:
            // Orientation
            var buf = new Float32Array(fieldBuf);
            var x = buf[0];
            var y = buf[1];
            return [SF.ORIENT, vector(x, y, 0)];
        case SF.VEL:
            // Velocity
            var buf = new Float32Array(fieldBuf);
            return [SF.VEL, buf[0]];
        default:
            console.log("Unknown state field type");
            break;
    }
    return undefined;
};


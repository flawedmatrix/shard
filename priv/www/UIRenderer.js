var UIRenderer = function() {
    // Get the canvas, context,and dimensions
    var canvas = this.canvas = document.getElementById("game-ui");
    if (!canvas.getContext) {
        alert("Your browser doesn't support canvas.");
        return;
    }
    var ctx = this.ctx = canvas.getContext("2d");
    this.width = this.canvas.width;
    this.height = this.canvas.height;

    var rect = canvas.getBoundingClientRect();
    this.offX = rect.left;
    this.offY = rect.top;
};

UIRenderer.prototype.drawGrid = function() {
    var WIDTH = this.width;
    var HEIGHT = this.height;
    var ctx = this.ctx;
    // Draw some grid on there (I'm lazy)
    // Vertical lines first
    ctx.strokeStyle = "#555555";
    ctx.lineWidth = 1;
    for (var i = 0; i <= 10; i++) {
        var xPos = i * WIDTH/10;
        ctx.beginPath();
        ctx.moveTo(xPos, 0);
        ctx.lineTo(xPos, HEIGHT);
        ctx.closePath();
        ctx.stroke();
    }
    // Horizontal lines next
    for (var j = 0; j <= 10; j++) {
        var yPos = j * HEIGHT/10;
        ctx.beginPath();
        ctx.moveTo(0, yPos);
        ctx.lineTo(WIDTH, yPos);
        ctx.closePath();
        ctx.stroke();
    }
};

var pathPolygon = function(ctx, polyArr, scale_x, scale_y) {
    ctx.beginPath();
    var start_x = begin_x = polyArr[0][0] * scale_x;
    var start_y = begin_y = polyArr[0][1] * scale_y;
    ctx.moveTo(start_x, start_y);
    for (var i = 1; i < polyArr.length; i += 1) {
        var next_x = polyArr[i][0] * scale_x;
        var next_y = polyArr[i][1] * scale_y;
        ctx.lineTo(next_x, next_y);
        start_x = next_x;
        start_y = next_y;
    }
    // Close the polygon
    ctx.lineTo(begin_x, begin_y);
    ctx.closePath();
}
UIRenderer.prototype.drawRegions = function() {
    var scale_x = this.width / 10;
    var scale_y = this.height / 10;
    var ctx = this.ctx;
    ctx.globalAlpha = 0.3;
    ctx.lineWidth = 5;
    var region = [];

    // Draw the green region
    // In grid coordinates in CCW order:
    region = [[0, 0], [0, 3], [3, 5], [5, 2], [9, 0]];
    ctx.fillStyle = ctx.strokeStyle = "green";
    pathPolygon(ctx, region, scale_x, scale_y);
    ctx.fill();
    ctx.stroke();

    // Red region
    region = [[5, 2], [3, 5], [4, 7], [7, 7], [7, 4]];
    ctx.fillStyle = ctx.strokeStyle = "red";
    pathPolygon(ctx, region, scale_x, scale_y);
    ctx.fill();
    ctx.stroke();

    // Blue region
    region = [[5, 2], [5, 5], [7, 5], [7, 4]];
    ctx.fillStyle = ctx.strokeStyle = "blue";
    pathPolygon(ctx, region, scale_x, scale_y);
    ctx.fill();
    ctx.stroke();

    // Orange region
    region = [[5, 5], [5, 7], [7, 7], [7, 5]];
    ctx.fillStyle = ctx.strokeStyle = "orange";
    pathPolygon(ctx, region, scale_x, scale_y);
    ctx.fill();
    ctx.stroke();
}

UIRenderer.prototype.renderCircle = function(pos, color) {
    var ctx = this.ctx;
    var HEIGHT = this.height;
    ctx.globalAlpha = 0.5;
    ctx.beginPath();
    ctx.arc(pos.x, HEIGHT-pos.y, 5, 0, 2 * Math.PI, false);
    ctx.closePath();
    ctx.fillStyle = color;
    ctx.fill();
    ctx.lineWidth = 1;
    ctx.strokeStyle = "black";
    ctx.stroke();
    ctx.globalAlpha = 1.0;
};

UIRenderer.prototype.renderLine = function(p1, p2, color) {
    var ctx = this.ctx;
    var HEIGHT = this.height;
    ctx.beginPath();
    ctx.lineWidth = 1;
    ctx.strokeStyle = color;
    ctx.moveTo(p1.x, HEIGHT-p1.y);
    ctx.lineTo(p2.x, HEIGHT-p2.y);
    ctx.closePath();
    ctx.stroke();
};

UIRenderer.prototype.clearCanvas = function() {
    var ctx = this.ctx;
    // Store the current transformation matrix
    ctx.save();

    // Use the identity matrix while clearing the canvas
    ctx.setTransform(1, 0, 0, 1, 0, 0);
    ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

    // Restore the transform
    ctx.restore();
};

UIRenderer.prototype.render = function() {
    this.clearCanvas();
    this.drawGrid();
    this.drawRegions();
};

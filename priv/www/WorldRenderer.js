var WorldRenderer = function() {
    this.camera = new THREE.PerspectiveCamera(
        45, window.innerWidth / window.innerHeight, 1, 100000
    );
    this.camera.position.y = -40;
    this.camera.position.z = 0;
    this.camera.lookAt(vector(0, 0, 0));
    this.camera.up = vector(0, 0, 1);

    this.scene = new THREE.Scene();

    // TERRAIN
    var gt = THREE.ImageUtils.loadCompressedTexture( "textures/grasslight-big.dds" );
    var gg = new THREE.PlaneGeometry( 500, 500 );
    gg.applyMatrix( new THREE.Matrix4().makeTranslation(250, 250, 0) );
    var gm = new THREE.MeshPhongMaterial({ color: 0xffffff, specular: 0x222222, shininess: 100, map: gt, bumpMap: gt, bumpScale: 3, metal: true, side: THREE.DoubleSide });

    gt.repeat.set( 180, 180 );
    gt.wrapS = gt.wrapT = THREE.RepeatWrapping;
    gt.anisotropy = 4;

    this.terrain = new THREE.Mesh( gg, gm );
    this.terrain.receiveShadow = true;

    this.scene.add( this.terrain );

    // LIGHT
    var light = new THREE.AmbientLight( 0x404040 );
    this.addObjectToScene(light);

    var directionalLight = new THREE.DirectionalLight( 0xffffff, 1.0 );
    // Vector indicates direction, not position
    directionalLight.position.set(0, 0, 1);
    this.addObjectToScene(directionalLight);

    // SKYDOME
    var map = THREE.ImageUtils.loadTexture( "textures/skydome.jpg" );
    map.flipY = false;

    var skyGeo = new THREE.SphereGeometry( 25000, 16, 8, 0, Math.PI * 2, 0, Math.PI * 0.75 );
    var sky = new THREE.Mesh( skyGeo, new THREE.MeshBasicMaterial( { color: 0xffffff, side: THREE.BackSide, map: map } ) );
    sky.rotation.x = Math.PI / 2;
    this.addObjectToScene(sky);

    // FINAL RENDERER PREPARATION
    this.greenMaterial = new THREE.MeshLambertMaterial({
        color: 0x00ff00,
        ambient: 0x00ff00
    });

    this.renderer = new THREE.WebGLRenderer();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.domElement.style.position = "absolute";
    this.renderer.domElement.style.zIndex = 0;

    $("#game-container").append(this.renderer.domElement);

    $(window).resize($.proxy(this.resize, this));

    this.coneGeometry = new THREE.CylinderGeometry(0, 1, 2, 15, 15, false);
};

WorldRenderer.prototype.addObjectToScene = function(object) {
    this.terrain.add(object);
    object.updateMatrix();
    object.updateMatrixWorld(true);
};

WorldRenderer.prototype.addEntityToScene = function(entity, main) {
    var mesh = new THREE.Mesh(this.coneGeometry, this.greenMaterial);
    mesh.scale.set(1, 1, 1);
    mesh.castShadow = true;
    mesh.receiveShadow = false;
    this.terrain.add(mesh);
    entity.mesh = mesh;
    mesh.entity = entity;
    if (defined(main)) {
        mesh.add(this.camera);
    }
};

WorldRenderer.prototype.removeEntityFromScene = function(entity) {
    if (defined(entity.mesh)) {
        this.terrain.remove(entity.mesh);
    }
};

WorldRenderer.prototype.resize = function() {
    // notify the renderer of the size change
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    // update the camera
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
};

WorldRenderer.prototype.updateRenderableWorld = function() {
    var renderables = this.terrain.children;
    for (var i = 0; i < renderables.length; i += 1) {
        var renderable = renderables[i];
        if (renderable.hasOwnProperty("entity")) {

            renderable.entity.advanceRenderState();

            renderable.position.subVectors(
                renderable.entity.renderPos,
                vector(0, 0, -1)
            );
            var dir = renderable.entity.renderOrient;
            var default_dir = vector(0, 1, 0);
            var angle = default_dir.angleTo(dir);
            var cp = vector(0);
            cp.crossVectors(default_dir, dir);
            var n = cp.dot(vector(0, 0, 1));
            if (n < 0) {
                angle = -angle;
            }
            renderable.rotation.z = angle;
        }
    }
    this.camera.lookAt(vector(0, 0, 0));
};

WorldRenderer.prototype.render = function() {
    this.updateRenderableWorld();
    this.renderer.render(this.scene, this.camera);
};

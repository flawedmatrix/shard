var WorldState = function() {
    this.mainChar = new CharState();
    this.mainChar.setPhysicsHandler(this.enforcePhysics.bind(this));
    this.mainChar.setGroundCheck(this.groundCheck.bind(this));
    this.mainChar.renderDelay = 33;
    this.intChars = [];

    this.mh = new MessageHandler();
    this.mh.setMainIdFunc(this.setMainId.bind(this));
    this.mh.setSnapshotsFunc(this.addSnapshots.bind(this));
    this.mh.setTerminateFunc(this.terminate.bind(this));

    this.ui_rndr = new UIRenderer();
    this.ws_rndr = new WorldRenderer();

    this.ws_rndr.addEntityToScene(this.mainChar, true);

    this.keys = {
        "up": false,
        "left": false,
        "right": false,
        "down": false
    };

    this.environObjs = [];
    this.initializeObjects();
};

WorldState.prototype.initializeObjects = function() {
    // Plane
    this.environObjs.push(this.ws_rndr.terrain);

    // Walls
    var wg = new THREE.PlaneGeometry(500, 10);
    var wm1 = new THREE.MeshBasicMaterial({ color: 0xffffff, side: THREE.DoubleSide });
    var wm2 = new THREE.MeshBasicMaterial({ color: 0xff0000, side: THREE.DoubleSide });
    var wm3 = new THREE.MeshBasicMaterial({ color: 0x00ff00, side: THREE.DoubleSide });
    var wm4 = new THREE.MeshBasicMaterial({ color: 0x0000ff, side: THREE.DoubleSide });

    var wall1 = new THREE.Mesh(wg, wm1);
    var wall2 = new THREE.Mesh(wg, wm2);
    var wall3 = new THREE.Mesh(wg, wm3);
    var wall4 = new THREE.Mesh(wg, wm4);

    wall1.rotation.x = Math.PI / 2;

    wall2.rotation.z = Math.PI / 2;
    wall2.rotation.y = Math.PI / 2;

    wall3.rotation.x = Math.PI / 2;

    wall4.rotation.z = Math.PI / 2;
    wall4.rotation.y = Math.PI / 2;

    wall1.position.set(250, 500, 5);
    wall2.position.set(0, 250, 5);
    wall3.position.set(250, 0, 5);
    wall4.position.set(500, 250, 5);

    this.addObjectToScene(wall1);
    this.addObjectToScene(wall2);
    this.addObjectToScene(wall3);
    this.addObjectToScene(wall4);

    // Box in the middle
    var bg = new THREE.CubeGeometry(50, 50, 10);
    var bm = new THREE.MeshBasicMaterial({ color: 0xffff00, side: THREE.DoubleSide });
    var bmesh = new THREE.Mesh(bg, bm );
    bmesh.position.set(250, 250, 0);
    this.addObjectToScene(bmesh);
};

WorldState.prototype.addObjectToScene = function(object) {
    this.environObjs.push(object);
    this.ws_rndr.addObjectToScene(object);
};

WorldState.prototype.enforcePhysics = function(pos, vel) {
    // Apply the same transform to the player
    var dir = vel.clone().normalize();
    var newVel = vel.clone();
    var maxDist = vel.length();

    var ray = new THREE.Raycaster(pos, dir);
    ray.precision = 0;
    var intersects = ray.intersectObjects(this.environObjs, false);
    if (intersects.length > 0) {
        if (intersects[0].distance >= maxDist) {
            return newVel;
        }
        var excessLength = maxDist - intersects[0].distance;
        var truncLength = intersects[0].distance;
        var remVel = newVel.clone().setLength(excessLength);

        if (!defined(intersects[0].face)) {
            console.log("Detected incompatible object");
            return newVel;
        }
        // Project the extra V onto the face
        var normal = intersects[0].face.normal.clone();
        normal.applyQuaternion(intersects[0].object.quaternion).normalize();
        remVel.projectOnPlane(normal);
        var finalVel = newVel.setLength(truncLength - 9e-6);
        console.log(pos);
        console.log(finalVel);
        finalVel.add(remVel);
        if (this.groundCheck(pos, finalVel)) {
            finalVel.setZ(0);
        }
        return finalVel;
    } else {
        return vel;
    }
};

WorldState.prototype.groundCheck = function(pos, vel) {
    if (defined(vel)) {
        vell = vel.length();
    } else {
        vell = 1e-5;
    }
    var ray = new THREE.Raycaster(pos, vector(0, 0, -1));
    ray.precision = 0;
    var intersects = ray.intersectObjects(this.environObjs, false);
    if (intersects.length > 0) {
        return intersects[0].distance < vell;
    } else {
        return false;
    }
};

WorldState.prototype.setMainId = function(id) {
    if (!defined(this.mainId)) {
        this.mainId = id;
    }
};

WorldState.prototype.run = function() {
    this.acc_dt = 0.0;
    this.lastTime = getTimestamp();
    this.lastUpdate = getTimestamp();
    this.tickTimer = window.setInterval(
        this.simulationTick.bind(this),
        game.TICKINT
    );
    this.render();
};

WorldState.prototype.terminate = function() {
    window.clearInterval(this.tickTimer);
};

WorldState.prototype.addSnapshots = function(snapshots) {
    if (!defined(this.mainId) || !snapshots.hasOwnProperty(this.mainId)) {
        console.log("This state packet is invalid");
        return;
    }

    var i = 0;
    var deletedItems = [];
    var currKeys = Object.keys(this.intChars);
    var snapshotKeys = Object.keys(snapshots);

    for (i = 0; i < currKeys.length; i += 1) {
        var currKey = currKeys[i];
        if (!snapshots.hasOwnProperty(currKey)) {
            deletedItems.push(currKey);
        }
    }
    for (i = 0; i < deletedItems.length; i += 1) {
        this.ws_rndr.removeEntityFromScene(this.intChars[deletedItems[i]]);
        delete this.intChars[deletedItems[i]];
    }
    for (i = 0; i < snapshotKeys.length; i += 1) {
        var sId = snapshotKeys[i];
        var snapshot = snapshots[sId];
        if (sId == this.mainId) {
            this.mainChar.servState = snapshot;
        } else {
            if (!this.intChars.hasOwnProperty(sId)) {
                this.intChars[sId] = new CharState();
                this.ws_rndr.addEntityToScene(this.intChars[sId]);
            }
            this.intChars[sId].stateBuffer.push(snapshot);
        }
    }
    if (!defined(this.running)) {
        this.run();
        this.running = true;
    }
};

WorldState.prototype.move = function(direction, keydown) {
    this.keys[direction] = keydown;
    this.mh.sendMovePacket(direction, keydown);
};

WorldState.prototype.orient = function(lookAt) {
    var direction = vector();
    direction.subVectors(lookAt, this.mainChar.pos);
    if (packVector && unpackVector) {
        var servDir = unpackVector(packVector(direction));
    } else {
        var servDir = direction;
    }
    this.mainChar.orient(servDir);
    this.mh.sendOrientPacket(direction);
};

WorldState.prototype.rotateView = function(lateral, vertical) {
    var a = new THREE.Euler( 0, 0, -lateral/450, 'ZXY' );
    this.ws_rndr.camera.position.applyAxisAngle(vector(1, 0, 0), -vertical/450);
    this.ws_rndr.camera.position.clamp(vector(0, -40, 0), vector(0, -0.5, 40));
    this.mainChar.orientation.applyEuler(a);
    this.mh.sendOrientPacket(this.mainChar.orientation);
};

WorldState.prototype.jump = function() {
    this.mainChar.jump(this.keys);
    this.mh.sendJumpPacket();
};

WorldState.prototype.action1 = function() {
    this.mh.sendAction1Packet();
};
WorldState.prototype.action2 = function() {
    this.mh.sendAction2Packet();
};
WorldState.prototype.useItem = function(item) {
    this.mh.sendItemUsePacket(item);
};

WorldState.prototype.sendChat = function() {
    this.mh.sendChatPacket("Hello, how are you doing today?");
};

WorldState.prototype.drawChar = function(chr, color) {
    if (defined(chr.renderPos) && defined(chr.renderOrient)) {
        var pos = chr.renderPos.clone();
        var orient = chr.renderOrient.clone();
    } else {
        var pos = chr.pos.clone();
        var orient = chr.orientation.clone();
    }
    orient.multiplyScalar(5);
    var line_end = vector();
    line_end.addVectors(pos, orient);
    this.ui_rndr.renderCircle(pos, color);
    this.ui_rndr.renderLine(pos, line_end, color);
};

WorldState.prototype.simulationTick = function() {
    var now = getTimestamp();
    var difft = (now - this.lastTime)/game.TICKINT;
    var dt = difft + this.acc_dt;
    var update_time = this.lastUpdate;
    while (dt >= 1.0) {
        update_time += game.TICKINT;
        this.mainChar.moveStep(this.keys, update_time);
        dt -= 1.0;
    }
    this.lastUpdate = update_time;
    this.acc_dt = dt;
    this.lastTime = now;
};

WorldState.prototype.render = function() {
    this.ui_rndr.render();
    this.ws_rndr.render();
    this.drawChar(this.mainChar, "red");

    if (defined(this.mainChar.servState)) {
        this.drawChar(this.mainChar.servState, "blue");
    }
    if (defined(this.mainChar.pserv)) {
       this.drawChar(this.mainChar.pserv, "magenta");
    }

    var currIds = Object.keys(this.intChars);
    for (var i = 0; i < currIds.length; i += 1) {
        var intChar = this.intChars[currIds[i]];
        this.drawChar(intChar, "green");
    }
    requestAnimationFrame(this.render.bind(this));
};

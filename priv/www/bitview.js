function InvalidArgumentsException(message) {
   this.message = message;
   this.name = "InvalidArgumentsException";
}

BitView = function(buf) {
    this.buffer = buf;
    this.u8 = new Uint8Array(buf);
};

BitView.prototype.getBit = function(idx) {
    var v = this.u8[idx >> 3];
    var off = idx & 0x7;
    return (v >> (7 - off)) & 1;
};

BitView.prototype.setBit = function(idx, val) {
    var off = idx & 0x7;
    if (val) {
        this.u8[idx >> 3] |= (0x80 >> off);
    } else {
        this.u8[idx >> 3] &= ~(0x80 >> off);
    }
};

BitView.prototype.getSub8NBits = function(idx, n) {
    var source = this.u8[idx >> 3];
    var off = idx & 7;
    return (source >>> (8 - off - n)) & ~(~0 << n);
};

BitView.prototype.setSub8NBits = function(idx, n, y) {
    var byteIdx = idx >> 3;
    var source = this.u8[byteIdx];
    var msk = ~(~0 << n);
    var dsp = (8 - (idx & 7) - n);
    this.u8[byteIdx] = (source & ~(msk << dsp)) | ((y & msk) << dsp);
};
var vector = function(x, y, z) {
    if (arguments.length === 1) {
        return new THREE.Vector3(x, x, x);
    } else {
        return new THREE.Vector3(x, y, z);
    }
};

var isVectorZero = function(vector) {
    var lsq = vector.lengthSq();
    return (lsq < 1.0e-5);
};

var str2u8a = function(str) {
    var buf = new Uint8Array(str.length);
    for (var i = 0; i < str.length; i++) {
        buf[i] = str.charCodeAt(i) & 0xFF;
    }
    return buf;
};

var u8a2str = function(u8a) {
    return String.fromCharCode.apply(null, u8a);
};

if (window.performance.now) {
    console.log("Using high performance timer");
    getTimestamp = function() { return window.performance.now(); };
} else {
    if (window.performance.webkitNow) {
        console.log("Using webkit high performance timer");
        getTimestamp = function() { return window.performance.webkitNow(); };
    } else {
        console.log("Using low performance timer");
        getTimestamp = function() { return new Date().getTime(); };
    }
}

function defined(x) {
    return (typeof x !== "undefined");
}


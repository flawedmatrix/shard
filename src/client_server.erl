-module(client_server).

%%-------------------------------------------------------------------
%% API Function Exports
%%-------------------------------------------------------------------
-export([start_link/1, init/2]).
%%-------------------------------------------------------------------
%% Required OTP Exports
%%-------------------------------------------------------------------
-export([
    system_code_change/4, system_continue/3,
    system_terminate/4, write_debug/3,
    notify/3
]).

%%-------------------------------------------------------------------
%% Mnesia exports
%%-------------------------------------------------------------------
-export([
    create_table/1,
    delete_table/0
]).

-include("globals.hrl").

-record(client_state, {
    id              :: integer(),
    pstate          :: tuple(),

    update_timer    :: reference(),
    update_int      :: integer(),

    tick_timer      :: reference(),
    last_time       :: erlang:timestamp(),
    acc_dt          :: number(),

    last_pong       :: erlang:timestamp()
}).

%%-------------------------------------------------------------------
%% API Function Definitions
%%-------------------------------------------------------------------
%% @doc Starts a new process synchronously. Spawns the process and
%%% waits for it to start.
%% See http://www.erlang.org/doc/man/proc_lib.html
start_link(Identifier) ->
    proc_lib:start_link(?MODULE, init, [self(), Identifier]).

%%-------------------------------------------------------------------
%% API Function Definitions
%%-------------------------------------------------------------------
%% @doc Notifies the parent of a successful start and then runs the
%% main loop. When the process has started, it must call
%% init_ack(Parent,Ret) or init_ack(Ret), where Parent is the
%% process that evaluates this function (see start_link/0 above).
%% At this time, Ret is returned.
init(Parent, Id) ->
    Debug = sys:debug_options([]),
    process_flag(trap_exit, true),

    true = gproc:reg({n, l, {?MODULE, Id}}),

    % Initialize the client state and its update timer
    NewState = new_client(Id),

    proc_lib:init_ack(Parent, {ok, self()}),
    loop(Parent, Debug, NewState).

%% @doc Abstraction for sending messages to the client server.
notify(Id, From, Msg) ->
    case gproc:lookup_local_name({?MODULE, Id}) of
        undefined -> ok;
        _P ->
            gproc:send({n, l, {?MODULE, Id}}, {From, Msg})
    end.

%%-------------------------------------------------------------------
%% Internal Functions
%%-------------------------------------------------------------------
%% @doc Our main loop, designed to handle system messages.
loop(Parent, Debug, State) ->
    write_state(State),
    receive
        {system, From, Request} ->
            sys:handle_system_msg(
                Request, From, Parent, ?MODULE, Debug, State
            );
        {'EXIT', Parent, Reason} ->
            terminate(Reason, Parent, Debug, State);
        Msg ->
            NewState = server_handle(Msg, State),
            loop(Parent, Debug, NewState)
    end.

server_handle({client, Packet}, CState) ->
    Decoded = ws_codec:decode(Packet),
    _NewState = handle(Decoded, CState);

server_handle({server, {char_pos, Pos}}, CState) ->
    PState = CState#client_state.pstate,
    NewPState = player_state:apply_state_change({position, set, vector:new(Pos)}, PState),
    CState#client_state {pstate = NewPState};

server_handle(update, CState) ->
    Timer = CState#client_state.update_timer,
    UpdateInt = CState#client_state.update_int,
    erlang:cancel_timer(Timer),
    NewTimer = erlang:send_after(UpdateInt, self(), update),
    NewCState = CState#client_state{update_timer = NewTimer},

    send_client_tick(CState),
    NewCState;

server_handle(tick, CState) ->
    Timer = CState#client_state.tick_timer,
    erlang:cancel_timer(Timer),
    NewTimer = erlang:send_after(?MS_PER_ITICK, self(), tick),
    TState = CState#client_state{tick_timer = NewTimer},

    simulation_tick(TState);

server_handle(_, CState) ->
    CState.

terminate(Reason, _Parent, _Debug, State) ->
    Id = State#client_state.id,
    lager:debug("Client process terminated for client ~w", [Id]),
    remove_client(Id),
    remove_state(Id),
    exit(Reason).

%% @doc Called by sys:handle_debug().
write_debug(_Dev, Event, Name) ->
    lager:debug("~p event = ~p", [Name, Event]).

%% @doc http://www.erlang.org/doc/man/sys.html#Mod:system_continue-3
system_continue(Parent, Debug, State) ->
    loop(Parent, Debug, State).

%% @doc http://www.erlang.org/doc/man/sys.html#Mod:system_terminate-4
system_terminate(Reason, Parent, Debug, State) ->
    terminate(Reason, Parent, Debug, State).

%% @doc http://www.erlang.org/doc/man/sys.html#Mod:system_code_change-4
system_code_change(State, _Module, _OldVsn, _Extra) ->
    {ok, State}.

%%-------------------------------------------------------------------
%% Client State Handling Functions
%%-------------------------------------------------------------------
%% @doc Initializes a new client state, complete with its own updating
%%      timers.
new_client(Id) ->
    DefaultPosition = vector:new(250, 250),
    DefaultOrientation = vector:new(0, 1),
    PState = player_state:new(DefaultPosition, DefaultOrientation),
    zone_server:send_task(1, add_char, {Id}),
    #client_state {
        id = Id,
        pstate = PState,

        update_timer = erlang:send_after(2 * ?MS_PER_UTICK, self(), update),
        update_int = ?MS_PER_UTICK,

        tick_timer = erlang:send_after(?MS_PER_ITICK, self(), tick),
        last_time = os:timestamp(),
        acc_dt = 0.0,

        last_pong = os:timestamp()
    }.

remove_client(Id) ->
    zone_server:send_task(1, rm_char, {Id}).

simulation_tick(CState) ->
    Now = os:timestamp(),
    DiffT = ?TDIFF(Now, CState#client_state.last_time) / ?MS_PER_ITICK,
    Dt = DiffT + CState#client_state.acc_dt,

    {AccDt, NewCState} = timestep(Dt, CState),

    NewCState#client_state{acc_dt = AccDt, last_time = Now}.

timestep(Dt, CState) when Dt < 1.0 ->
    {Dt, CState};
timestep(Dt, CState) ->
    PState = player_state:tick(CState#client_state.pstate),
    timestep(Dt - 1.0, CState#client_state{pstate = PState}).

%%-------------------------------------------------------------------
%% Server-Sent Message Handlers
%%-------------------------------------------------------------------

%% @doc Sends a pong with the accepted update int
send_pong(NewUpdateInt, CState) ->
    Id = CState#client_state.id,
    PongPacket = ws_codec:encode({pong, NewUpdateInt}),
    ws_handler:to_client(Id, {client, PongPacket}).

%% @doc The main client state updating function.
%%      On every tick, the state and neighboring states are encoded
%%      and then sent to the client.
send_client_tick(CState) ->
    % On every tick encode the state and send it
    Id = CState#client_state.id,
    LastPong = CState#client_state.last_pong,
    TickInt = ?MS_PER_ITICK,
    Time = trunc(?TDIFF(os:timestamp(), LastPong)),
    ClientFields = [
        {tickint, TickInt},
        {time, Time},
        {id, Id}
    ],
    StateFields = player_state:export_state(CState#client_state.pstate),

    NeighborStates = get_neighbors(Id),
    NeighborFields = neighbor_fields(NeighborStates, []),

    Fields = ClientFields ++ StateFields ++ NeighborFields,
    PackedState = ws_codec:encode({state, Fields}),
    ws_handler:to_client(Id, {client, PackedState}).


%% @doc Given a list of neighbors, grab the relevant player state.
neighbor_fields([], Fields) ->
    Fields;
neighbor_fields([CState|Rest], Fields) ->
    Id = CState#client_state.id,
    ClientFields = [{id, Id}],
    StateFields = player_state:export_state(CState#client_state.pstate),
    neighbor_fields(Rest, Fields ++ ClientFields ++ StateFields).

%%-------------------------------------------------------------------
%% Decoded Client to Server Message Handlers
%%-------------------------------------------------------------------
handle(action1, CState) ->
    PState = CState#client_state.pstate,
    NewPState = player_state:action1(PState),
    CState#client_state{pstate = NewPState};
handle({orient, Direction}, CState) ->
    % PState = CState#client_state.pstate,
    % player_move:orient(Direction, PState),
    % CState#client_state{pstate = NewPState};
    Id = CState#client_state.id,
    zone_server:send_task(1, orient_char, {Id, Direction}),
    CState;

handle({move, Dir, Start}, CState) ->
    % PState = CState#client_state.pstate,
    % NewPState = player_move:move(Dir, Start, PState),
    % CState#client_state{pstate = NewPState};
    Id = CState#client_state.id,
    zone_server:send_task(1, move_char, {Id, Dir, Start}),
    CState;

handle(jump, CState) ->
    % PState = CState#client_state.pstate,
    % NewPState = player_move:jump(PState),
    % CState#client_state{pstate = NewPState};
    Id = CState#client_state.id,
    zone_server:send_task(1, jump_char, {Id}),
    CState;

handle({ping, ReqUpdateInt}, CState) ->
    if ReqUpdateInt =< 33 ->
        NewUpdateInt = 33;
    ReqUpdateInt >= 300 ->
        NewUpdateInt = 300;
    true ->
        NewUpdateInt = ReqUpdateInt
    end,
    send_pong(NewUpdateInt, CState),
    CState#client_state{
        update_int = NewUpdateInt,
        last_pong = os:timestamp()
    };

handle({chat, _Params, ChatMsg}, CState) ->
    % For now broadcast it back to the player.
    Id = CState#client_state.id,
    Username = <<"Enterprising">>,
    EncodedChatMsg = ws_codec:encode({chat, Username, ChatMsg}),
    ws_handler:to_client(Id, {client, EncodedChatMsg}),
    CState;

handle(unknown, CState) ->
    CState;

handle(_, CState) ->
    CState.

%%-------------------------------------------------------------------
%% Mnesia handlers
%%-------------------------------------------------------------------

%% @doc The responsibility for creating a table for client_state records
%%      is deferred to the client state module.
create_table(Nodes) ->
    {atomic, ok} = mnesia:create_table(client_state, [
        {attributes, record_info(fields, client_state)},
        {ram_copies, Nodes}
    ]).

%% @doc Writes the client state directly into the Mnesia table.
write_state(CState) ->
    F = fun() -> mnesia:write(CState) end,
    mnesia:activity(transaction, F).

%% @doc Performs a query to match neighboring client states.
%%      Currently, this does a naive "Get all states that are not me"
%%      But this can be changed to grab ones in surrounding regions later.
get_neighbors(Id) ->
    MatchHead = #client_state{id='$1', _='_'},
    Guard = {'=/=', '$1', Id},
    Result = '$_',
    F = fun() ->
        mnesia:select(client_state, [{MatchHead, [Guard], [Result]}])
    end,
    mnesia:activity(async_dirty, F).

%% @doc Removes this state from the table so that it is no longer seen.
remove_state(Id) ->
    F = fun() -> mnesia:delete(client_state, Id, write) end,
    mnesia:activity(transaction, F).

%% @doc Cleans up by removing the table created at the start of the server.
delete_table() ->
    mnesia:delete_table(client_state).


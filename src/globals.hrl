% Client update heartbeat interval
-define(MS_PER_UTICK, 33).
% Internal update heartbeat interval
-define(MS_PER_ITICK, 33).

-define(PING_INT, 5000).

-define(EPSILON, 1.0e-10).

-record(vector, {
    vx = 0      :: number(),
    vy = 0      :: number(),
    vz = 0      :: number()
}).

% Calculates T2 - T1
-define(TDIFF(T2, T1), timer:now_diff(T2, T1)/1000).

-define(GRAVITY, 0.2).

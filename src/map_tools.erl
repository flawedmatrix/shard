-module(map_tools).

-include("globals.hrl").

-record(region, {
    id          :: any(),
    verts       :: list()
}).

-export([init_map/0, region_id/1, get_regions/3]).

init_map() ->
    GreenVerts = [{0, 0}, {0, 3}, {3, 5}, {5, 2}, {9, 0}],
    RedVerts = [{5, 2}, {3, 5}, {4, 7}, {7, 7}, {7, 4}],
    BlueVerts = [{5, 2}, {5, 5}, {7, 5}, {7, 4}],
    OrangeVerts = [{5, 5}, {5, 7}, {7, 7}, {7, 5}],

    GreenRgn = #region{id = green, verts = transform(GreenVerts)},
    RedRgn = #region{id = red, verts = transform(RedVerts)},
    BlueRgn = #region{id = blue, verts = transform(BlueVerts)},
    OrangeRgn = #region{id = orange, verts = transform(OrangeVerts)},
    [GreenRgn, RedRgn, BlueRgn, OrangeRgn].

region_id(Region) ->
    Region#region.id.

get_regions(Pt, [Rgn | Rest], Regions) ->
    InRgn = pnpoly(Pt, Rgn#region.verts),
    case InRgn of
        true ->
            NewRegions = Regions ++ [Rgn];
        false ->
            NewRegions = Regions
    end,
    get_regions(Pt, Rest, NewRegions);

get_regions(_Pt, [], Regions) ->
    Regions.

transform(Verts) ->
    lists:map(fun({X, Y}) -> vector:new(X * 50.0, 500 - Y * 50.0, 0) end, Verts).

pnpoly(Point, Vertices = [P1 | Rest]) ->
    IsVertex = lists:member(Point, Vertices),
    case IsVertex of
        true ->
            true;
        false ->
            OnBorders = test_borders(Point, Vertices ++ [P1]),
            case OnBorders of
                true ->
                    true;
                false ->
                    FirstAbove = P1#vector.vy =< Point#vector.vy,
                    test_inside(Point, Vertices ++ [P1], FirstAbove, false)
            end
    end.

test_borders(RefPt = #vector{vx = RefX, vy = RefY}, [P1, P2 | Rest]) ->
    #vector{vx = MaxX, vy = MaxY} = vector:vmax(P1, P2),
    #vector{vx = MinX, vy = MinY} = vector:vmin(P1, P2),

    %% Not on border cases
    Fail = (RefX < MinX) or (RefX > MaxX) or (RefY < MinY) or (RefY > MaxY),
    OnLine = point_on_line(Fail, RefPt, P1, P2),
    case OnLine of
        true ->
            true;
        false ->
            test_borders(RefPt, [P2 | Rest])
    end;

test_borders(_Poly, _RefPt) ->
    false.

%% (IsFail, RefPt, P1, P2)
point_on_line(true, _RefPt, _P1, _P2) ->
    false;
point_on_line(false, RefPt, P1, P2) ->
    abs(vector:cross_product_2d(RefPt, P1, P2)) =< ?EPSILON.

test_inside(RefPt, [P1, P2 | Rest], LastAbove, Inside) ->
    ThisAbove = P2#vector.vy =< RefPt#vector.vy,
    case (ThisAbove =:= LastAbove) of
        true ->
            NewInside = Inside;
        false ->
            #vector{vx = T2X, vy = T2Y} = vector:sub(P2, RefPt),
            #vector{vx = DX, vy = DY} = vector:sub(P1, P2),
            CP = T2Y * DX - T2X * DY,
            NewInside = swap_on_cp(CP, Inside, ThisAbove)
    end,
    test_inside(RefPt, [P2 | Rest], ThisAbove, NewInside);

test_inside(_RefPt, _Vertices, _LastAbove, Inside) ->
    Inside.

swap_on_cp(CP, Inside, ThisAbove) when (CP >= 0) =:= ThisAbove ->
    not Inside;
swap_on_cp(_CP, Inside, _ThisAbove) ->
    Inside.


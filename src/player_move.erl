-module(player_move).

%% Buff API Exports
-export([init/0, refresh/3, callback/1, start/2, tick/2, stop/2]).

%% API
-export([orient/2, move/3, jump/1]).

-include("globals.hrl").
-include("player_state.hrl").

-record(buff_state, {}).

%%-------------------------------------------------------------------
%% Buff API
%%-------------------------------------------------------------------

%% @doc Required method for initializing the buff before it starts.
%% Since this is a permament buff it never really needs to use it.
init() ->
    #buff_state{}.

%% @doc Handles what happens when the buff is refreshed
refresh(_NewBuffState, _OldBuffState, _PState) ->
    #buff_state{}.

%% @doc Required callback for what happens when the buff is ticked.
%% Since this is a permament buff it always ticks
callback(_BuffState) ->
    tick.

%% @doc Handles what happens when the buff is newly started
start(_InitBuffState, _PState) ->
    {[], #buff_state{}}.

%% @doc The meat of the buff: Called on every server tick
tick(BuffState, PState) ->
    P0 = PState#player_state.position,

    IsFalling = player_state:flag_exists(falling, PState),
    if IsFalling ->
        {Dv} = player_state:get_flag(falling, PState),
        NewDv = vector:sub(Dv, vector:new(0, 0, ?GRAVITY)),

        {Pt, CDv} = enforce_physics(Dv, P0),
        ST0 = [{position, add, CDv}],
        case is_on_ground(Pt) of
            true ->
                ST = ST0 ++ [{remove_flag, falling}];
            _ ->
                ST = ST0 ++ [{add_flag, falling, {NewDv}}]
        end;
    true ->
        Speed = PState#player_state.speed,
        % Get the current velocity of the player
        NDir = direction(PState),
        Dv = vector:scalar_mult_vector(Speed, NDir),

        {_Pt, CDv} = enforce_physics(Dv, P0),
        ST = [{position, add, CDv}]
    end,
    % Return the new state transaction
    {ST, BuffState}.

%% @doc Handles what happens when the buff is stopped
stop(_BuffState, _PState) ->
    [].

%%-------------------------------------------------------------------
%% Movement Helpers
%%-------------------------------------------------------------------

add_dir(up, Dir, Acc) ->
    vector:add(Acc, Dir);
add_dir(down, Dir, Acc) ->
    vector:add(Acc, vector:neg(Dir));
add_dir(left, #vector{vx = Vx, vy = Vy}, Acc) ->
    vector:add(Acc, vector:new(-Vy, Vx));
add_dir(right, #vector{vx = Vx, vy = Vy}, Acc) ->
    vector:add(Acc, vector:new(Vy, -Vx));
add_dir(_, _, Acc) ->
    Acc.

direction(PState) ->
    Dir = PState#player_state.orientation,
    Keys = PState#player_state.keys_down,
    accum_directions(Dir, Keys).

accum_directions(_Dir, {none, none, none, none}) ->
    vector:new(0);
accum_directions(_Dir, {up, left, down, right}) ->
    vector:new(0);
accum_directions(Dir, {Up, Left, Down, Right}) ->
    Acc = vector:new(0),
    UpXORDown = (Up =:= up) xor (Down =:= down),
    LeftXORRight = (Left =:= left) xor (Right =:= right),
    case UpXORDown of
        true ->
            case Up of
                up ->
                    Acc1 = add_dir(Up, Dir, Acc);
                _ ->
                    Acc1 = add_dir(Down, Dir, Acc)
            end;
        false ->
            Acc1 = Acc
    end,
    case LeftXORRight of
        true ->
            case Left of
                left ->
                    Acc2 = add_dir(Left, Dir, Acc1);
                _ ->
                    Acc2 = add_dir(Right, Dir, Acc1)
            end;
        false ->
            Acc2 = Acc1
    end,
    vector:normalize(Acc2).

orient({Vx, Vy, _Vz}, PState) ->
    PState#player_state{orientation = vector:new(Vx, Vy)}.

move(Dir, State, PState) ->
    Keys = PState#player_state.keys_down,
    case State of
        start ->
            NewKeys = player_state:add_key(Dir, Keys);
        stop ->
            NewKeys = player_state:remove_key(Dir, Keys)
    end,
    PState#player_state{keys_down = NewKeys}.

jump(PState) ->
    IsFalling = player_state:flag_exists(falling, PState),
    if not IsFalling ->
        Speed = PState#player_state.speed,
        NDir = direction(PState),
        Dv = vector:scalar_mult_vector(Speed, NDir),
        JumpDv = Dv#vector{vz = 1},
        player_state:add_flag(falling, {JumpDv}, PState);
    true ->
        PState
    end.

enforce_physics(Velocity, Position) ->
    NewPos = vector:add(Position, Velocity),
    ClampedPos = vector:clamp(NewPos, vector:new(0), vector:new(500)),
    Diff = vector:sub(ClampedPos, NewPos),
    ClampedVel = vector:add(Velocity, Diff),
    {ClampedPos, ClampedVel}.

is_on_ground(#vector{vz = Vz}) ->
    Vz < 1.0e-5.


-module(player_state).

-export([action1/1]).

-export([
    new/2, new/4, tick/1,
    add_key/2, remove_key/2,
    add_buff/3, buff_exists/2,
    add_flag/3, get_flag/2, remove_flag/2, flag_exists/2,
    export_state/1, apply_state_transaction/2, apply_state_change/2
]).

-include("globals.hrl").
-include("player_state.hrl").

%%-------------------------------------------------------------------
%% Player State Handling Functions
%%-------------------------------------------------------------------
%% @doc Creates a new player state
new(Position, Orientation) ->
    new(Position, Orientation, gb_trees:empty(), gb_trees:empty()).

new(Position, Orientation, Flags, Buffs) ->
    PState = #player_state {
        position = Position,
        orientation = Orientation,
        speed = 1.0,
        flags = Flags,
        buffs = Buffs,
        keys_down = {none, none, none, none},
        map = map_tools:init_map()
    },
    init(PState).

init(PState) ->
    PState1 = add_buff(player_move, [], PState),
    PState1.

%% @doc Key state manager helpers
add_key(up, {_, Left, Down, Right}) ->
    {up, Left, Down, Right};
add_key(left, {Up, _, Down, Right}) ->
    {Up, left, Down, Right};
add_key(down, {Up, Left, _, Right}) ->
    {Up, Left, down, Right};
add_key(right, {Up, Left, Down, _}) ->
    {Up, Left, Down, right}.

remove_key(up, {_, Left, Down, Right}) ->
    {none, Left, Down, Right};
remove_key(left, {Up, _, Down, Right}) ->
    {Up, none, Down, Right};
remove_key(down, {Up, Left, _, Right}) ->
    {Up, Left, none, Right};
remove_key(right, {Up, Left, Down, _}) ->
    {Up, Left, Down, none}.

%% @doc The main loop iteration of the player state. Everything happens here.
tick(PState) ->
    BuffDict = PState#player_state.buffs,
    Buffs = gb_trees:keys(BuffDict),
    {STs, NewBuffDict} = process_buffs(Buffs, [], BuffDict, PState),
    NewPState = PState#player_state{buffs = NewBuffDict},
    apply_state_transaction(STs, NewPState).

%%-------------------------------------------------------------------
%% Player State Command Functions
%%-------------------------------------------------------------------
action1(PState) ->
    PState.
    % add_buff(ticker_buff, [500], PState).

%%-------------------------------------------------------------------
%% Buff and Flag Handling Functions
%%-------------------------------------------------------------------

add_buff(Buff, Args, PState) ->
    BuffDict = PState#player_state.buffs,
    Exists = gb_trees:is_defined(Buff, BuffDict),
    SBS = apply(Buff, init, Args),
    case Exists of
        true ->
            BS = gb_trees:get(Buff, BuffDict),
            NewBuffState = Buff:refresh(SBS, BS, PState),
            NewBuffDict = gb_trees:update(Buff, NewBuffState, BuffDict);
        false ->
            NewBuffDict = gb_trees:insert(Buff, SBS, BuffDict);
        _ ->
            NewBuffDict = BuffDict
    end,
    PState#player_state{buffs = NewBuffDict}.

add_flag(Flag, State, PState) ->
    NewFlagDict = gb_trees:enter(Flag, State, PState#player_state.flags),
    PState#player_state{flags = NewFlagDict}.

get_flag(Flag, PState)->
    gb_trees:get(Flag, PState#player_state.flags).

remove_flag(Flag, PState) ->
    NewFlagDict = gb_trees:delete_any(Flag, PState#player_state.flags),
    PState#player_state{flags = NewFlagDict}.

%% @doc Tests whether or not BuffKey is an active buff.
buff_exists(Buff, PState) ->
    gb_trees:is_defined(Buff, PState#player_state.buffs).

%% @doc Tests whether or not BuffKey is an active buff.
flag_exists(Flag, PState) ->
    gb_trees:is_defined(Flag, PState#player_state.flags).

process_buffs([], STs, BuffDict, _PState) ->
    {STs, BuffDict};
process_buffs([Buff | Rest], STs, BuffDict, PState) ->
    BuffState = gb_trees:get(Buff, BuffDict),
    Callback = Buff:callback(BuffState),
    case Callback of
        stop ->
            ST = Buff:stop(BuffState, PState),
            NewBuffDict = gb_trees:delete(Buff, BuffDict);
        _ ->
            {ST, NewBuffState} = Buff:Callback(BuffState, PState),
            NewBuffDict = gb_trees:update(Buff, NewBuffState, BuffDict)
    end,
    process_buffs(Rest, STs ++ ST, NewBuffDict, PState).

%% @doc This function takes in a state and a state transaction
%% It applies to the state each change in the state transaction
%% A state change transaction is a list of state changes pending to be made
%% Ex: [{position, add, {Dx, Dy}}, {field, operation, DeltaState}]
%% This function will also be responsible for verifying that this move is valid
apply_state_transaction([], PState) ->
    PState;

apply_state_transaction([Change | Rest], PState) ->
    NewState = apply_state_change(Change, PState),
    Rgns = map_tools:get_regions(
        NewState#player_state.position,
        NewState#player_state.map,
        []
    ),
    _RgnNames = lists:map(fun(Region) -> map_tools:region_id(Region) end, Rgns),
    %% lager:debug("Player regions ~w", [RgnNames]),
    apply_state_transaction(Rest, NewState).

apply_state_change({position, set, Pos}, PState) ->
    PState#player_state { position = Pos };

apply_state_change({position, add, Dv}, PState) ->
    % Grab the old position
    Pos = PState#player_state.position,
    % Calculate the new position
    NewPos = vector:add(Pos, Dv),
    % lager:debug("Position change from ~p to ~p", [Pos, NewPos]),
    % Calculate the new player state
    PState#player_state { position = NewPos };

apply_state_change({position, sub, Dv}, PState) ->
    apply_state_change({position, add, vector:neg(Dv)}, PState);

apply_state_change({orientation, set, Dir}, PState) ->
    %% lager:debug("Orientation change"),
    PState#player_state { orientation = Dir };

apply_state_change({add_buff, Buff, Args}, PState) ->
    add_buff(Buff, Args, PState);
apply_state_change({add_flag, Flag, State}, PState) ->
    add_flag(Flag, State, PState);
apply_state_change({remove_flag, Flag}, PState) ->
    remove_flag(Flag, PState);

apply_state_change(_, PState) ->
    PState.

%% @doc This method serializes the player state to be sent to the client
export_state(PState) ->
    Position = PState#player_state.position,
    Orientation = PState#player_state.orientation,
    Speed = PState#player_state.speed,
    #vector{vx = Px, vy = Py, vz = Pz} = Position,
    #vector{vx = Vx, vy = Vy} = Orientation,
    [{position, Px, Py, Pz}, {orientation, Vx, Vy}, {velocity, Speed}].


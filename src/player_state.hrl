-record(player_state, {
    position    :: #vector{},
    orientation :: #vector{},
    speed       :: number(),
    flags       :: gb_trees:tree(),
    buffs       :: gb_trees:tree(),
    keys_down   :: tuple(),
    map         :: list()
}).


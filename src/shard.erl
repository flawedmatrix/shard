-module(shard).

%% API.
-export([start/0]).

start() ->
    %% Apparently without this line it won't save anything to disk
    %% mnesia:create_schema([node()]),
    {ok, Started} = application:ensure_all_started(shard).
    %% ok = application:start(crypto),
    %% ok = application:start(cowlib),
    %% ok = application:start(ranch),
    %% ok = application:start(cowboy),
    %% ok = application:start(compiler),
    %% ok = application:start(syntax_tools),
    %% ok = application:start(goldrush),
    %% ok = application:start(lager),
    %% ok = application:start(gproc),
    %% ok = application:start(shard).


-module(shard_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    lager:set_loglevel(lager_console_backend, debug),
    gproc:reg_shared({c, l, id}, 1),
    %% Blocks until all child processes are initialized and started
    {ok, SPID} = shard_sup:start_link(),
    %% {ok, SSPID} = shard_sup:start_link([1]),
    %% Initialize the client server tables
    {atomic, ok} = client_server:create_table([node()]),
    initialize_zones(),
    {ok, SPID}.

initialize_zones() ->
    Id = gproc:get_value_shared({c, l, id}),
    lager:debug("Starting zone server ~w", [Id]),
    gproc:update_shared_counter({c, l, id}, 1),
    {ok, _ZoneID} = supervisor:start_child(world_sup, [Id]),
    ok.

stop(_State) ->
    client_server:delete_table(),
    ok.

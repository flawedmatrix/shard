-module(ticker_buff).

%% Buff API Exports
-export([init/1, refresh/3, callback/1, start/2, tick/2, stop/2]).

-include("globals.hrl").

-record(buff_state, {
    time_left   :: number(),
    callback    :: atom()
}).

%% @doc Required method for initializing the buff before it starts.
init(TimeLeft) ->
    #buff_state{time_left = TimeLeft, callback = start}.

% Handles what happens when the buff is refreshed
refresh(NewBuffState, BuffState, _PState) ->
    lager:debug("Timer reset"),
    NewTimeLeft = NewBuffState#buff_state.time_left,
    BuffState#buff_state{time_left = NewTimeLeft}.

%% @doc Required callback for what happens when the buff is ticked.
callback(BuffState) ->
    BuffState#buff_state.callback.

%% @doc Handles what happens when the buff is newly started
start(BuffState, _PState) ->
    TimeLeft = BuffState#buff_state.time_left,
    lager:debug("Timer started with ~w", [TimeLeft]),
    {[], BuffState#buff_state{callback = tick}}.

tick(BuffState, _PState) ->
    TimeLeft = BuffState#buff_state.time_left,
    NewTimeLeft = TimeLeft - ?MS_PER_ITICK,
    lager:debug("Time left: ~w", [NewTimeLeft]),

    case NewTimeLeft of
        _ when NewTimeLeft < 0.0 ->
            NewCB = stop;
        _ ->
            NewCB = tick
    end,
    {[], BuffState#buff_state{time_left = NewTimeLeft, callback = NewCB}}.


stop(_BuffState, _PState) ->
    lager:debug("Timer stopped."),
    [].


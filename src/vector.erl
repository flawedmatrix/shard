-module(vector).

-include("globals.hrl").

-export([
    new/1, new/2, new/3, clamp/3, add/2, sub/2, neg/1, vmax/2, vmin/2,
    normalize/1, scalar_mult_vector/2,
    cross_product_2d/3
]).

new({Vx, Vy, Vz}) ->
    #vector{vx = Vx, vy = Vy, vz = Vz};
new(Vxyz) ->
    #vector{vx = Vxyz, vy = Vxyz, vz = Vxyz}.

new(Vx, Vy) ->
    #vector{vx = Vx, vy = Vy}.

new(Vx, Vy, Vz) ->
    #vector{vx = Vx, vy = Vy, vz = Vz}.

add(
    #vector{vx = Vx1, vy = Vy1, vz = Vz1},
    #vector{vx = Vx2, vy = Vy2, vz = Vz2}
) ->
    #vector{vx = Vx1 + Vx2, vy = Vy1 + Vy2, vz = Vz1 + Vz2}.

sub(
    #vector{vx = Vx1, vy = Vy1, vz = Vz1},
    #vector{vx = Vx2, vy = Vy2, vz = Vz2}
) ->
    #vector{vx = Vx1 - Vx2, vy = Vy1 - Vy2, vz = Vz1 - Vz2}.

neg(#vector{vx = Vx, vy = Vy, vz = Vz}) ->
    #vector{vx = -Vx, vy = -Vy, vz = -Vz}.

%% Arguments: VToClamp, VMin, VMax
clamp(
    #vector{vx = Px, vy = Py, vz = Pz},
    #vector{vx = MinX, vy = MinY, vz = MinZ},
    #vector{vx = MaxX, vy = MaxY, vz = MaxZ}
) ->
    #vector{
        vx = clamp(Px, MinX, MaxX),
        vy = clamp(Py, MinY, MaxY),
        vz = clamp(Pz, MinZ, MaxZ)
    };

clamp(Num, Min, _) when Num < Min ->
    Min;
clamp(Num, _, Max) when Num > Max ->
    Max;
clamp(Num, _, _) ->
    Num.

vmax(
    #vector{vx = Vx1, vy = Vy1, vz = Vz1},
    #vector{vx = Vx2, vy = Vy2, vz = Vz2}
) ->
    #vector{vx = max(Vx1, Vx2), vy = max(Vy1, Vy2), vz = max(Vz1, Vz2)}.

vmin(
    #vector{vx = Vx1, vy = Vy1, vz = Vz1},
    #vector{vx = Vx2, vy = Vy2, vz = Vz2}
) ->
    #vector{vx = min(Vx1, Vx2), vy = min(Vy1, Vy2), vz = min(Vz1, Vz2)}.

normalize(Vector = #vector{vx = Vx, vy = Vy, vz = Vz})
    when abs(Vx) < ?EPSILON, abs(Vy) < ?EPSILON, abs(Vz) < ?EPSILON ->
    Vector;
normalize(#vector{vx = Vx, vy = Vy, vz = Vz}) ->
    Magnitude = math:sqrt((Vx * Vx) + (Vy * Vy) + (Vz * Vz)),
    #vector{vx = Vx/Magnitude, vy = Vy/Magnitude, vz = Vz/Magnitude}.

scalar_mult_vector(Scalar, #vector{vx = Vx, vy = Vy, vz = Vz}) ->
    #vector{vx = Scalar * Vx, vy = Scalar * Vy, vz = Scalar * Vz}.

cross_product_2d(V1, V2, V3) ->
    #vector{vx = D12X, vy = D12Y} = vector:sub(V1, V2),
    #vector{vx = D32X, vy = D32Y} = vector:sub(V3, V2),
    D12X * D32Y - D12Y * D32X.

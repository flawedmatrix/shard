-module(web_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type, Args), {I, {I, start_link, Args}, permanent, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init(_Args) ->
    %% {Host, list({Path, Handler, Opts})}
    %% Dispatch the requests (whatever the host is) to
    %% erws_handler, without any additional options.
    Dispatch = cowboy_router:compile([
        {'_', [
            {
                "/",
                cowboy_static,
                {priv_file, shard, "www/index.html"}
            },
            {
                "/websocket",
                ws_handler,
                []
            },
            {
                "/[...]",
                cowboy_static,
                {priv_dir, shard, "www"}
            }
        ]}
    ]),
    %% Name, NbAcceptors, Transport, TransOpts, Protocol, ProtoOpts
    %% Listen in 5000/tcp for http connections.
    ListenerSpec = ranch:child_spec(
        shard_http_listener, 100,
        ranch_tcp, [{port, 1337}, {shutdown, infinity}],
        cowboy_protocol, [{env, [{dispatch, Dispatch}]}]
    ),
    {ok, {
        {one_for_one, 10, 10},
        [
            ?CHILD(cowboy_clock, worker, []),
            ?CHILD(ranch_sup, supervisor, []),
            ListenerSpec
        ]
    }}.


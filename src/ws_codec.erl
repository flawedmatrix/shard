-module(ws_codec).

-export([decode/1, encode/1]).

%%-------------------------------------------------------------------
%% Packet headers for incoming packets
%%-------------------------------------------------------------------
-define(I_MOVEMENT_PACKET,  0).
-define(I_ACTION1_PACKET,   1).
-define(I_ACTION2_PACKET,   2).
-define(I_ITEM_USE_PACKET,  3).
-define(I_ORIENT_PACKET,    5).
-define(I_PING_PACKET,      6).
-define(I_CHAT_PACKET,      7).
-define(I_JUMP_PACKET,      8).

%%-------------------------------------------------------------------
%% Packet headers for outgoing packets
%%-------------------------------------------------------------------
-define(O_STATE_PACKET,     0).
-define(O_ACK_PACKET,       4).
-define(O_PONG_PACKET,      6).
-define(O_CHAT_PACKET,      7).

%%-------------------------------------------------------------------
%% State fields types for state sync packets
%%-------------------------------------------------------------------
%% State field types for client parameters
-define(SF_ID_FIELD,        0).
-define(SF_TICKINT_FIELD,   1).
-define(SF_TIME_FIELD,      2).

%% State field types for entity parameters
-define(SF_POS_FIELD,       101).
-define(SF_ORIENT_FIELD,    102).
-define(SF_VEL_FIELD,       103).


-define(ARROW_UP,           0).
-define(ARROW_LEFT,         1).
-define(ARROW_DOWN,         2).
-define(ARROW_RIGHT,        3).

%%-------------------------------------------------------------------
%% Export Function
%%-------------------------------------------------------------------
%% @doc Whenever a packet is received, it is sent to this function
%% to be decoded to a message that will be sent to the server-side
%% client process
decode(<<?I_MOVEMENT_PACKET:8, Arrow:2, KeyUp:1, _Rest/bits>>) ->
    % lager:debug("Received movement packet arrow: ~p", [Arrow]),
    Command = case KeyUp of 0 -> stop; 1 -> start end,
    % lager:debug("Movement ~p", [Command]),
    case Arrow of
        ?ARROW_UP ->
            AtomArrow = up;
            % lager:debug("Movement arrow is up");
        ?ARROW_LEFT ->
            AtomArrow = left;
            % lager:debug("Movement arrow is left");
        ?ARROW_DOWN ->
            AtomArrow = down;
            % lager:debug("Movement arrow is down");
        ?ARROW_RIGHT ->
            AtomArrow = right;
            % lager:debug("Movement arrow is right");
        _ ->
            AtomArrow = up,
            lager:debug("Movement arrow isn't correct")
    end,
    {move, AtomArrow, Command};

decode(<<?I_ACTION1_PACKET:8, _Rest/bits>>) ->
    lager:debug("Received action 1 packet"),
    action1;
decode(<<?I_ACTION2_PACKET:8, _Rest/bits>>) ->
    lager:debug("Received action 2 packet"),
    action2;
decode(<<?I_ITEM_USE_PACKET:8, ItemId:16, _Rest/bits>>) ->
    lager:debug("Received item use packet: ~p", [ItemId]),
    {item_use, ItemId};
decode(<<?I_ORIENT_PACKET:8, PackedVector:16, _Rest/bits>>) ->
    % lager:debug("Received orient packet: ~p", [PackedVector]),
    Vector = unpack_vector(<<PackedVector:16>>),
    {orient, Vector};
decode(<<?I_PING_PACKET:8, ReqUpdateInt:16, _Rest/bits>>) ->
    {ping, ReqUpdateInt};
decode(<<?I_CHAT_PACKET:8, Params:8, ChatMsg/binary>>) ->
    lager:debug("Received chat packet: ~s", [ChatMsg]),
    {chat, Params, ChatMsg};
decode(<<?I_JUMP_PACKET:8, _Rest/bits>>) ->
    jump;
decode(<<Opcode:8, Rest/bits>>) ->
    lager:debug("Received unknown packet opcode ~w, rest ~w", [Opcode, Rest]),
    unknown;
decode(Packet) ->
    lager:debug("Okay, something seriously went wrong. Received: ~w", [Packet]),
    unknown.

%%-------------------------------------------------------------------
%% Export Function
%%-------------------------------------------------------------------
%% @doc Whenever a packet is to be sent, the state is sent to this
%% function to be encoded into a binary string that is sent as a
%% packet.
encode({state, StateFields}) ->
    encode_state(StateFields, <<0:8>>);
encode({ack}) ->
    <<?O_ACK_PACKET:8>>;
encode({chat, Username, Message}) ->
    % TODO: Add 5 bit parameter field
    UnameLen = bit_size(Username) bsr 3,
    <<?O_CHAT_PACKET:8, 0:8, UnameLen:8, Username/binary, Message/binary>>;
encode({pong, UpdateInt}) ->
    <<?O_PONG_PACKET:8, UpdateInt:16/little-integer>>.

encode_state([], PackedState) ->
    PackedState;
encode_state([StateField | Rest], PackedState) ->
    Field = encode_field(StateField),
    encode_state(Rest, <<PackedState/binary, Field/binary>>).


encode_field({id, Id}) ->
    <<?SF_ID_FIELD:8, 1:8, Id:8/integer>>;
encode_field({tickint, TickInt}) ->
    <<?SF_TICKINT_FIELD:8, 1:8, TickInt:8/integer>>;
encode_field({time, Time}) ->
    <<?SF_TIME_FIELD:8, 2:8, Time:16/little-integer>>;

encode_field({position, X, Y, Z}) ->
    Data = <<X:32/little-float, Y:32/little-float, Z:32/little-float>>,
    <<?SF_POS_FIELD:8, 12:8, Data/binary>>;
encode_field({orientation, X, Y}) ->
    Data = <<X:32/little-float, Y:32/little-float>>,
    <<?SF_ORIENT_FIELD:8, 8:8, Data/binary>>;
encode_field({velocity, Velocity}) ->
    Data = <<Velocity:32/little-float>>,
    <<?SF_VEL_FIELD:8, 4:8, Data/binary>>;
encode_field(Field) ->
    lager:debug("Unknown field: ~w", [Field]),
    <<>>.
%%-------------------------------------------------------------------
%% Internal Function
%%-------------------------------------------------------------------
%% @doc Unpacks a vector from its packed float representation to a
%% 3-tuple
unpack_vector(<<XSign:1, X:7, YSign:1, Y:6, ZSign:1>>) ->
    %% If we directly do a reverse transform, we'll get a
    %% Point on the plane (0,0), (0, 126), (126, 0)
    %% We need a point on the unit sphere, so we need to
    %% normalize the point so that x^2 + y^2 + z^2 = 1
    if
        (X + Y) >= 127 ->
            UPX = 127 - X,
            UPY = 127 - Y;
        true ->
            UPX = X,
            UPY = Y
    end,
    {NX, NY, NZ} = normalize(UPX, UPY, 127 - UPX - UPY),
    FX = case XSign of 1 -> -NX; 0 -> NX end,
    FY = case YSign of 1 -> -NY; 0 -> NY end,
    FZ = case ZSign of 1 -> -NZ; 0 -> NZ end,
    {FX, FY, FZ}.

normalize(X, Y, Z) ->
    W = 1 / math:sqrt(X*X + Y*Y + Z*Z),
    {W*X, W*Y, W*Z}.

%%-------------------------------------------------------------------
%% Internal Function
%%-------------------------------------------------------------------
%% @doc Packs a vector from its 3-tuple representation to a packed float
%% representation
pack_vector({X, Y, Z}) ->
    XSign = if X < 0 -> AbsX = -X, 1; true -> AbsX = X, 0 end,
    YSign = if Y < 0 -> AbsY = -Y, 1; true -> AbsY = Y, 0 end,
    ZSign = if Z < 0 -> AbsZ = -Z, 1; true -> AbsZ = Z, 0 end,
    W = 126.0 / (AbsX + AbsY + AbsZ),
    % Make sure 0 <= XBits < 127
    XBits = trunc(AbsX * W),
    % Make sure 0 <= YBits < 127
    YBits = trunc(AbsY * W),
    PXBits = if YBits >= 64 -> 127 - XBits; true -> XBits end,
    PYBits = if YBits >= 64 -> 127 - YBits; true -> YBits end,
    <<XSign:1, PXBits:7, YSign:1, PYBits:6, ZSign:1>>.


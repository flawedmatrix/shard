-module(ws_handler).
-behaviour(cowboy_websocket_handler).

-export([
    init/3, to_client/2,
    websocket_init/3, websocket_handle/3,
    websocket_info/3, websocket_terminate/3
]).

-record(state, {id, cpid}).

-define(BKEY, {?MODULE, broadcast}).

%% @doc Called to know how to dispatch a new connection.
init({tcp, http}, Req, _Opts) ->
    lager:debug("Request: ~p", [Req]),
    % "upgrade" every request to websocket,
    % we're not interested in serving any other content.
    {upgrade, protocol, cowboy_websocket}.

%% @doc Abstraction for sending messages to the client.
to_client(Id, Msg) ->
    gproc:send({n, l, {?MODULE, Id}}, Msg).

%% @doc Called for every new websocket connection.
websocket_init(_TransportName, Req, []) ->
    Id = gproc:get_value_shared({c, l, id}),
    lager:debug("New WS connection has started for client ~w", [Id]),
    gproc:update_shared_counter({c, l, id}, 1),
    %% Gproc notation: {p, l, Name} means {(p)roperty, (l)ocal, Name}
    gproc:reg({n, l, {?MODULE, Id}}),
    gproc:reg({p, l, ?BKEY}),
    % Start a new client_monit under client_sup and monitor that process
    process_flag(trap_exit, true),
    {ok, ChildPID} = supervisor:start_child(client_sup, [Id]),
    erlang:monitor(process, ChildPID),
    State = #state{id = Id, cpid = ChildPID},
    {ok, Req, State, 30000}.

%% @doc Called when a binary message arrives.
websocket_handle({binary, Msg}, Req, State) ->
    % <<Opcode:3, Rest/bits>> = Msg,
    Id = State#state.id,
    % lager:debug("Received binary: Opcode = ~w, Rest = ~w", [Opcode, Rest]),
    client_server:notify(Id, client, Msg),
    {reply,
        {binary, ws_codec:encode({ack})},
        Req, State
    };

%% @doc Called when a text message arrives.
% websocket_handle({text, Msg}, Req, State) ->
%     {ok, Req, State};
websocket_handle(_Any, Req, State) ->
    {ok, Req, State}.

%% @doc Called when the client monitor shuts down
websocket_info({'DOWN', _MonitorRef, process, PID, _Info}, Req, State) 
    when PID =:= State#state.cpid ->
    {shutdown, Req, State};
%% @doc Called when the message is a message from the client server
websocket_info({client, BinaryEncodedMsg}, Req, State) ->
    {reply,
        {binary, BinaryEncodedMsg},
        Req, State
    };

% Other messages from the system are handled here.
% websocket_info({PID, ?BKEY, Msg}, Req, State) ->
%     SelfPID = self(),
%     case PID of
%         SelfPID ->
%             lager:debug("Received self message"),
%             {ok, Req, State};
%         _ ->
%             lager:debug("Websocket client received system message ~w", [Msg]),
%             {reply,
%                 {binary, << Msg/binary >>},
%                 Req, State
%             }
%     end;

%% @doc Called when the message is some unknown message
websocket_info(Info, Req, State) ->
    lager:debug("Websocket client received info ~w", [Info]),
    {ok, Req, State}.

%% @doc Called when the client monitor shuts down when websocket_info returns
websocket_terminate({normal, shutdown}, _Req, State) ->
    Id = State#state.id,
    lager:debug("Client server ~w is down. Terminating WS connection.", [Id]),
    ok;
%% @doc Called when the websocket connection shuts down for any other reason
websocket_terminate(_Reason, _Req, State) ->
    Id = State#state.id,
    ChildPID = State#state.cpid,
    lager:debug("Client ~w disconnected. Terminating client server.", [Id]),
    supervisor:terminate_child(client_sup, ChildPID).



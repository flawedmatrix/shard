-module(zone_server).

%%-------------------------------------------------------------------
%% API Function Exports
%%-------------------------------------------------------------------
-export([start_link/1, init/2]).
%%-------------------------------------------------------------------
%% Required OTP Exports
%%-------------------------------------------------------------------
-export([
    system_code_change/4, system_continue/3,
    system_terminate/4, write_debug/3,
    foo/1, add_char/1, rm_char/1, move_char/3,
    send_task/3
]).
-record(state, {id, port}).

%%-------------------------------------------------------------------
%% API Function Definitions
%%-------------------------------------------------------------------
%% @doc Starts a new process synchronously. Spawns the process and
%%% waits for it to start.
%% See http://www.erlang.org/doc/man/proc_lib.html
start_link(Identifier) ->
    proc_lib:start_link(?MODULE, init, [self(), Identifier]).

%%-------------------------------------------------------------------
%% API Function Definitions
%%-------------------------------------------------------------------
%% @doc Notifies the parent of a successful start and then runs the
%% main loop. When the process has started, it must call
%% init_ack(Parent,Ret) or init_ack(Ret), where Parent is the
%% process that evaluates this function (see start_link/0 above).
%% At this time, Ret is returned.
init(Parent, Id) ->
    Debug = sys:debug_options([]),
    process_flag(trap_exit, true),

    Port = try
        true = gproc:reg({n, l, {?MODULE, Id}}),
        PrivDir = code:priv_dir(shard),
        ExtPrg = filename:join([PrivDir, "physerv"]),
        open_port({spawn_executable, ExtPrg}, [{packet, 2}, binary])
    catch
        _:Reason ->
            lager:error("Unable to start zone server ~p: ~p", [Id, Reason]),
            proc_lib:init_ack(parent, {error, Reason}),
            exit(Reason)
    end,
    lager:debug("Zone server ~p successfully started!", [Id]),
    proc_lib:init_ack(Parent, {ok, self()}),
    loop(Parent, Debug, #state{
        id = Id,
        port = Port
    }).

%%-------------------------------------------------------------------
%% Internal Functions
%%-------------------------------------------------------------------

%% @doc Our main loop, designed to handle system messages.
loop(Parent, Debug, State) ->
    Port = State#state.port,
    receive
        {system, From, Request} ->
            sys:handle_system_msg(
                Request, From, Parent, ?MODULE, Debug, State
            );
        {'EXIT', Parent, Reason} ->
            terminate(Reason, State);
        {'EXIT', Port, _Reason} ->
            terminate(port_terminated, State);
        Msg ->
            NewState = server_handle(Msg, State),
            loop(Parent, Debug, NewState)
    end.

server_handle({Port, {data, Data}}, State) when Port =:= State#state.port ->
    Term = binary_to_term(Data),
    handle_data(Term),
    State;
server_handle({send_task, Task, Args}, State) ->
    State#state.port ! {self(), {command, term_to_binary({Task, Args})}},
    State;

server_handle(_, State) ->
    State.

handle_data({char_pos, Id, Pos}) ->
    client_server:notify(Id, server, {char_pos, Pos});
handle_data(X) ->
    lager:debug("From Physics Server: ~p", [X]),
    ok.

send_task(Id, Task, Args) ->
    case gproc:lookup_local_name({?MODULE, Id}) of
        undefined -> ok;
        _P ->
            gproc:send({n, l, {?MODULE, Id}}, {send_task, Task, Args})
    end.

foo(X) ->
    send_task(1, foo, {X}).

add_char(X) ->
    send_task(1, add_char, {X}).

rm_char(X) ->
    send_task(1, rm_char, {X}).

move_char(X, Dir, On) ->
    send_task(1, move_char, {X, Dir, On}).

terminate(normal, State) ->
    normal_terminate(State);
terminate(shutdown, State) ->
    normal_terminate(State);
terminate({shutdown, _Term}, State) ->
    normal_terminate(State);
terminate(Reason, _State) ->
    exit(Reason).

normal_terminate(State) ->
    Id = State#state.id,
    Port = State#state.port,
    Port ! {self(), close},
    receive
        {Port, closed} ->
            exit(normal)
    after 1000 ->
        lager:debug("Zone server ~p timed out on port process!", Id),
        exit(timeout)
    end.

%% @doc Called by sys:handle_debug().
write_debug(_Dev, Event, Name) ->
    lager:debug("~p event = ~p", [Name, Event]).

%% @doc http://www.erlang.org/doc/man/sys.html#Mod:system_continue-3
system_continue(Parent, Debug, State) ->
    loop(Parent, Debug, State).

%% @doc http://www.erlang.org/doc/man/sys.html#Mod:system_terminate-4
system_terminate(Reason, _Parent, _Debug, State) ->
    terminate(Reason, State).

%% @doc http://www.erlang.org/doc/man/sys.html#Mod:system_code_change-4
system_code_change(State, _Module, _OldVsn, _Extra) ->
    {ok, State}.
